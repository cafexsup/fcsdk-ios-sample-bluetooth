#!/usr/bin/perl -w
eval 'exec /usr/bin/perl -S $0 ${1+"$@"}'
  if 0;
$0 =~ s/^.*?(\w+)[\.\w+]*$/$1/;

use strict;
use Symbol;
use vars qw{ $isEOF $Hold %wFiles @Q $CondReg
	     $doAutoPrint $doOpenWrite $doPrint };
$doAutoPrint = 1;
$doOpenWrite = 1;
# prototypes
sub openARGV();
sub getsARGV(;\$);
sub eofARGV();
sub printQ();

# Run: the sed loop reading input and applying the script
#
sub Run(){
    my( $h, $icnt, $s, $n );
    # hack (not unbreakable :-/) to avoid // matching an empty string
    my $z = "\000"; $z =~ /$z/;
    # Initialize.
    openARGV();
    $Hold    = '';
    $CondReg = 0;
    $doPrint = $doAutoPrint;
CYCLE:
    while( getsARGV() ){
	chomp();
	$CondReg = 0;   # cleared on t
BOS:;
# #Certain strings we want to save 
# s/HMAC_SHA1/abcd_cdef/g
{ $s = s /HMAC_SHA1/abcd_cdef/sg;
  $CondReg ||= $s;
}
# s/h_main/lhi_kg/g
{ $s = s /h_main/lhi_kg/sg;
  $CondReg ||= $s;
}
# s/_Ios_Openmode/_Ios_Nickmode/g
{ $s = s /_Ios_Openmode/_Ios_Nickmode/sg;
  $CondReg ||= $s;
}
# s/_CONFLICT/_ZONFLICT/g
{ $s = s /_CONFLICT/_ZONFLICT/sg;
  $CondReg ||= $s;
}
# s/_CONVERSATION/_ZONVERSATION/g
{ $s = s /_CONVERSATION/_ZONVERSATION/sg;
  $CondReg ||= $s;
}
# s/_CC_SHA1/_CC_NWA1/g
{ $s = s /_CC_SHA1/_CC_NWA1/sg;
  $CondReg ||= $s;
}
# s/CALL_CONFIG/CALL_ZONFIG/g
{ $s = s /CALL_CONFIG/CALL_ZONFIG/sg;
  $CondReg ||= $s;
}
# s/_main/_nnnn/g
{ $s = s /_main/_nnnn/sg;
  $CondReg ||= $s;
}
# s/_SSL/_AAA/g
{ $s = s /_SSL/_AAA/sg;
  $CondReg ||= $s;
}
# s/_ssl23/_BBBBB/g
{ $s = s /_ssl23/_BBBBB/sg;
  $CondReg ||= $s;
}
# s/_ssl3/_CCCC/g
{ $s = s /_ssl3/_CCCC/sg;
  $CondReg ||= $s;
}
# s/_ssl2/_DDDD/g
{ $s = s /_ssl2/_DDDD/sg;
  $CondReg ||= $s;
}
# s/_ERR/_EEE/g
{ $s = s /_ERR/_EEE/sg;
  $CondReg ||= $s;
}
# s/_OPENSL/_FFFFFF/g
{ $s = s /_OPENSL/_FFFFFF/sg;
  $CondReg ||= $s;
}
# s/_CRYPTO/_GGGGGG/g
{ $s = s /_CRYPTO/_GGGGGG/sg;
  $CondReg ||= $s;
}
# s/_dtls/_HHHH/g
{ $s = s /_dtls/_HHHH/sg;
  $CondReg ||= $s;
}
# s/_tls/_III/g
{ $s = s /_tls/_III/sg;
  $CondReg ||= $s;
}
# s/_ssl/_JJJ/g
{ $s = s /_ssl/_JJJ/sg;
  $CondReg ||= $s;
}
# s/_PKCS/_KKKK/g
{ $s = s /_PKCS/_KKKK/sg;
  $CondReg ||= $s;
}
# s/_X509/_LLLL/g
{ $s = s /_X509/_LLLL/sg;
  $CondReg ||= $s;
}
# s/_sk_/_MM_/g
{ $s = s /_sk_/_MM_/sg;
  $CondReg ||= $s;
}
# s/_ENGINE/_NNNNNN/g
{ $s = s /_ENGINE/_NNNNNN/sg;
  $CondReg ||= $s;
}
# s/_engine/_NNnNNN/g
{ $s = s /_engine/_NNnNNN/sg;
  $CondReg ||= $s;
}
# s/_EVP/_OOO/g
{ $s = s /_EVP/_OOO/sg;
  $CondReg ||= $s;
}
# s/_ec_/_PPP/g
{ $s = s /_ec_/_PPP/sg;
  $CondReg ||= $s;
}
# s/_EC_/_QQQ/g
{ $s = s /_EC_/_QQQ/sg;
  $CondReg ||= $s;
}
# s/_ASN1_/_RRRRR/g
{ $s = s /_ASN1_/_RRRRR/sg;
  $CondReg ||= $s;
}
# s/_bn_/_SSS/g
{ $s = s /_bn_/_SSS/sg;
  $CondReg ||= $s;
}
# s/_BN_/_TTT/g
{ $s = s /_BN_/_TTT/sg;
  $CondReg ||= $s;
}
# s/_OBJ_/_UUU_/g
{ $s = s /_OBJ_/_UUU_/sg;
  $CondReg ||= $s;
}
# s/_OPENSSL/_VVVVVVV/g
{ $s = s /_OPENSSL/_VVVVVVV/sg;
  $CondReg ||= $s;
}
# s/_SHA256_/_WWWWWWW/g
{ $s = s /_SHA256_/_WWWWWWW/sg;
  $CondReg ||= $s;
}
# s/_SHA224_/_WVWWWWW/g
{ $s = s /_SHA224_/_WVWWWWW/sg;
  $CondReg ||= $s;
}
# s/_SHA384_/_WWVWWWW/g
{ $s = s /_SHA384_/_WWVWWWW/sg;
  $CondReg ||= $s;
}
# s/_SHA512_/_WWWVWWW/g
{ $s = s /_SHA512_/_WWWVWWW/sg;
  $CondReg ||= $s;
}
# s/_SHA1_/_XXXX_/g
{ $s = s /_SHA1_/_XXXX_/sg;
  $CondReg ||= $s;
}
# s/_SHA_/_XXX_/g
{ $s = s /_SHA_/_XXX_/sg;
  $CondReg ||= $s;
}
# s/_BUF_/_YYY_/g
{ $s = s /_BUF_/_YYY_/sg;
  $CondReg ||= $s;
}
# s/_lh_/_ZZ_/g
{ $s = s /_lh_/_ZZ_/sg;
  $CondReg ||= $s;
}
# s/_policy_/_aaaaaa_/g
{ $s = s /_policy_/_aaaaaa_/sg;
  $CondReg ||= $s;
}
# s/_POLICY_/_bbbbbb_/g
{ $s = s /_POLICY_/_bbbbbb_/sg;
  $CondReg ||= $s;
}
# s/_RAND/_cccc/g
{ $s = s /_RAND/_cccc/sg;
  $CondReg ||= $s;
}
# s/_BIO/_ddd/g
{ $s = s /_BIO/_ddd/sg;
  $CondReg ||= $s;
}
# s/_RSA/_eee/g
{ $s = s /_RSA/_eee/sg;
  $CondReg ||= $s;
}
# s/_DSA/_fff/g
{ $s = s /_DSA/_fff/sg;
  $CondReg ||= $s;
}
# s/_rsa/_eex/g
{ $s = s /_rsa/_eex/sg;
  $CondReg ||= $s;
}
# s/_dsa/_ffx/g
{ $s = s /_dsa/_ffx/sg;
  $CondReg ||= $s;
}
# s/_bf_/_ggg/g
{ $s = s /_bf_/_ggg/sg;
  $CondReg ||= $s;
}
# s/_Cam/_hhh/g
{ $s = s /_Cam/_hhh/sg;
  $CondReg ||= $s;
}
# s/_md4/_iii/g
{ $s = s /_md4/_iii/sg;
  $CondReg ||= $s;
}
# s/_md5/_jjj/g
{ $s = s /_md5/_jjj/sg;
  $CondReg ||= $s;
}
# s/_MD4/_ixi/g
{ $s = s /_MD4/_ixi/sg;
  $CondReg ||= $s;
}
# s/_MD5/_jxj/g
{ $s = s /_MD5/_jxj/sg;
  $CondReg ||= $s;
}
# s/_PB/_kk/g
{ $s = s /_PB/_kk/sg;
  $CondReg ||= $s;
}
# s/_UI_/_jj_/g
{ $s = s /_UI_/_jj_/sg;
  $CondReg ||= $s;
}
# s/_pqueue_/_llllll_/g
{ $s = s /_pqueue_/_llllll_/sg;
  $CondReg ||= $s;
}
# s/_pitem/_mmmmm/g
{ $s = s /_pitem/_mmmmm/sg;
  $CondReg ||= $s;
}
# s/_PEM/_nnn/g
{ $s = s /_PEM/_nnn/sg;
  $CondReg ||= $s;
}
# s/_CON/_nnn/g
{ $s = s /_CON/_nnn/sg;
  $CondReg ||= $s;
}
# s/_COM/_nn1/g
{ $s = s /_COM/_nn1/sg;
  $CondReg ||= $s;
}
# s/_OCS/_nn2/g
{ $s = s /_OCS/_nn2/sg;
  $CondReg ||= $s;
}
# s/_NCONF/_nnnnn/g
{ $s = s /_NCONF/_nnnnn/sg;
  $CondReg ||= $s;
}
# s/_v3/_nn/g
{ $s = s /_v3/_nn/sg;
  $CondReg ||= $s;
}
# s/_MDC/_nnn/g
{ $s = s /_MDC/_nnn/sg;
  $CondReg ||= $s;
}
# s/_HMA/_nnn/g
{ $s = s /_HMA/_nnn/sg;
  $CondReg ||= $s;
}
# s/_hma/_nnn/g
{ $s = s /_hma/_nnn/sg;
  $CondReg ||= $s;
}
# s/_RIP/_nnn/g
{ $s = s /_RIP/_nnn/sg;
  $CondReg ||= $s;
}
# s/_DES/_xnn/g
{ $s = s /_DES/_xnn/sg;
  $CondReg ||= $s;
}
# s/_AES/_nxn/g
{ $s = s /_AES/_nxn/sg;
  $CondReg ||= $s;
}
# s/_RC2/_nnx/g
{ $s = s /_RC2/_nnx/sg;
  $CondReg ||= $s;
}
# s/_BF/_xx/g
{ $s = s /_BF/_xx/sg;
  $CondReg ||= $s;
}
# s/_RC4/_xxo/g
{ $s = s /_RC4/_xxo/sg;
  $CondReg ||= $s;
}
# s/_DHP/_xoz/g
{ $s = s /_DHP/_xoz/sg;
  $CondReg ||= $s;
}
# s/_DH_/_xon/g
{ $s = s /_DH_/_xon/sg;
  $CondReg ||= $s;
}
# s/_i2d_/_xoxo/g
{ $s = s /_i2d_/_xoxo/sg;
  $CondReg ||= $s;
}
# s/_d2i/_oox/g
{ $s = s /_d2i/_oox/sg;
  $CondReg ||= $s;
}
# s/_o2i_/_Noxo/g
{ $s = s /_o2i_/_Noxo/sg;
  $CondReg ||= $s;
}
# s/_i2o/_ooN/g
{ $s = s /_i2o/_ooN/sg;
  $CondReg ||= $s;
}
# s/_ECDSA/_nxnoo/g
{ $s = s /_ECDSA/_nxnoo/sg;
  $CondReg ||= $s;
}
# s/_WHI/_zzz/g
{ $s = s /_WHI/_zzz/sg;
  $CondReg ||= $s;
}
# s/_X9/_x_/g
{ $s = s /_X9/_x_/sg;
  $CondReg ||= $s;
}
# s/_dh_/_iz_/g
{ $s = s /_dh_/_iz_/sg;
  $CondReg ||= $s;
}
# s/_DSO/_azz/g
{ $s = s /_DSO/_azz/sg;
  $CondReg ||= $s;
}
# s/_UTF8/_azza/g
{ $s = s /_UTF8/_azza/sg;
  $CondReg ||= $s;
}
# s/_CBIG/_azza/g
{ $s = s /_CBIG/_azza/sg;
  $CondReg ||= $s;
}
# s/_LONG/_azza/g
{ $s = s /_LONG/_azza/sg;
  $CondReg ||= $s;
}
# s/_ZLON/_azza/g
{ $s = s /_ZLON/_azza/sg;
  $CondReg ||= $s;
}
# s/_GENE/_azza/g
{ $s = s /_GENE/_azza/sg;
  $CondReg ||= $s;
}
# s/_Open/_azza/g
{ $s = s /_Open/_azza/sg;
  $CondReg ||= $s;
}
# s/_BIGN/_xzza/g
{ $s = s /_BIGN/_xzza/sg;
  $CondReg ||= $s;
}
# s/_asn1/_xzzq/g
{ $s = s /_asn1/_xzzq/sg;
  $CondReg ||= $s;
}
# s/_x509/_xzzp/g
{ $s = s /_x509/_xzzp/sg;
  $CondReg ||= $s;
}
# s/_a2i/_zzp/g
{ $s = s /_a2i/_zzp/sg;
  $CondReg ||= $s;
}
# s/_whi/_xzz/g
{ $s = s /_whi/_xzz/sg;
  $CondReg ||= $s;
}
# s/_cleanse/_aleanse/g
{ $s = s /_cleanse/_aleanse/sg;
  $CondReg ||= $s;
}
# s/_check_defer/_aheck_defer/g
{ $s = s /_check_defer/_aheck_defer/sg;
  $CondReg ||= $s;
}
# s/_obj_cleanup/_bbb_cleanup/g
{ $s = s /_obj_cleanup/_bbb_cleanup/sg;
  $CondReg ||= $s;
}
# s/_ripemd/_ripemc/g
{ $s = s /_ripemd/_ripemc/sg;
  $CondReg ||= $s;
}
# s/_ECPKPA/_CCCXCC/g
{ $s = s /_ECPKPA/_CCCXCC/sg;
  $CondReg ||= $s;
}
# s/_ECDH/_CCCX/g
{ $s = s /_ECDH/_CCCX/sg;
  $CondReg ||= $s;
}
# s/_DISPLAYTEXT/_DIXPLAYTEST/g
{ $s = s /_DISPLAYTEXT/_DIXPLAYTEST/sg;
  $CondReg ||= $s;
}
# s/_ECPARAMETERS_it/_nCPARAMETERS_it/g
{ $s = s /_ECPARAMETERS_it/_nCPARAMETERS_it/sg;
  $CondReg ||= $s;
}
# s/_ECPKParameters_print/_ECPKParameters_prinx/g
{ $s = s /_ECPKParameters_print/_ECPKParameters_prinx/sg;
  $CondReg ||= $s;
}
# s/_ecdsa_check/_ecdsa_checx/g
{ $s = s /_ecdsa_check/_ecdsa_checx/sg;
  $CondReg ||= $s;
}
# s/_ecdh_check/_ecdh_checx/g
{ $s = s /_ecdh_check/_ecdh_checx/sg;
  $CondReg ||= $s;
}
# s/_app_pkey_methods/_app_pkey_methodx/g
{ $s = s /_app_pkey_methods/_app_pkey_methodx/sg;
  $CondReg ||= $s;
}
# s/_evp_pkey_set_cb_translate/_evp_pkey_set_cb_translatx/g
{ $s = s /_evp_pkey_set_cb_translate/_evp_pkey_set_cb_translatx/sg;
  $CondReg ||= $s;
}
# s/_DIRECTORYSTRING_it/_DIRECTORYSTRING_ix/g
{ $s = s /_DIRECTORYSTRING_it/_DIRECTORYSTRING_ix/sg;
  $CondReg ||= $s;
}
# s/_pem_check_suffix/_pem_check_suffni/g
{ $s = s /_pem_check_suffix/_pem_check_suffni/sg;
  $CondReg ||= $s;
}
# s/_EXTENDED_KEY_USAGE_it/_EXTENDED_KEY_USAGE_ix/g
{ $s = s /_EXTENDED_KEY_USAGE_it/_EXTENDED_KEY_USAGE_ix/sg;
  $CondReg ||= $s;
}
# s/_name_cmp/_name_cmJ/g
{ $s = s /_name_cmp/_name_cmJ/sg;
  $CondReg ||= $s;
}
# s/_hex_to_string/_hex_to_strinx/g
{ $s = s /_hex_to_string/_hex_to_strinx/sg;
  $CondReg ||= $s;
}
# s/_string_to_hex/_string_to_hen/g
{ $s = s /_string_to_hex/_string_to_hen/sg;
  $CondReg ||= $s;
}
# s/_EDIPARTYNAME_it/_EDIPARTYNAME_ix/g
{ $s = s /_EDIPARTYNAME_it/_EDIPARTYNAME_ix/sg;
  $CondReg ||= $s;
}
# s/_OTHERNAME_it/_OTHERNAME_xx/g
{ $s = s /_OTHERNAME_it/_OTHERNAME_xx/sg;
  $CondReg ||= $s;
}
# s/_OTHERNAME_new/_OTHERNAME_nxx/g
{ $s = s /_OTHERNAME_new/_OTHERNAME_nxx/sg;
  $CondReg ||= $s;
}
# s/_PKEY_USAGE_PERIOD/_PKEY_USAGE_PERIxx/g
{ $s = s /_PKEY_USAGE_PERIOD/_PKEY_USAGE_PERIxx/sg;
  $CondReg ||= $s;
}
# s/_SXNET/_SXNET/g
{ $s = s /_SXNET/_SXNET/sg;
  $CondReg ||= $s;
}
# s/_SXNETID_it/_SXNETID_ij/g
{ $s = s /_SXNETID_it/_SXNETID_ij/sg;
  $CondReg ||= $s;
}
# s/_SXNET_add_id_INTEGER/_SXNET_add_id_INTEXER/g
{ $s = s /_SXNET_add_id_INTEGER/_SXNET_add_id_INTEXER/sg;
  $CondReg ||= $s;
}
# s/_NOTICEREF/_NOTICERij/g
{ $s = s /_NOTICEREF/_NOTICERij/sg;
  $CondReg ||= $s;
}
# s/_USERNOTICE/_USERNOTIix/g
{ $s = s /_USERNOTICE/_USERNOTIix/sg;
  $CondReg ||= $s;
}
# s/_POLICYQUALINFO_free/_POLICYQUALINFO_frex/g
{ $s = s /_POLICYQUALINFO_free/_POLICYQUALINFO_frex/sg;
  $CondReg ||= $s;
}
# s/_POLICYQUALINFO_it/_POLICYQUALINFO_ix/g
{ $s = s /_POLICYQUALINFO_it/_POLICYQUALINFO_ix/sg;
  $CondReg ||= $s;
}
# s/_POLICYINFO_free/_POLICYINFO_frex/g
{ $s = s /_POLICYINFO_free/_POLICYINFO_frex/sg;
  $CondReg ||= $s;
}
# s/_POLICYINFO/_POLICYINix/g
{ $s = s /_POLICYINFO/_POLICYINix/sg;
  $CondReg ||= $s;
}
# s/_CERTIFICATEPOLICIES_it/_CERTIFICATEPOLICIES_ix/g
{ $s = s /_CERTIFICATEPOLICIES_it/_CERTIFICATEPOLICIES_ix/sg;
  $CondReg ||= $s;
}
# s/_DIST_POINT_set_dpname/_DIST_POINT_set_dpname/g
{ $s = s /_DIST_POINT_set_dpname/_DIST_POINT_set_dpname/sg;
  $CondReg ||= $s;
}
# s/_ISSUING_DIST_POINT_free/_ISSUING_DIST_POINT_frex/g
{ $s = s /_ISSUING_DIST_POINT_free/_ISSUING_DIST_POINT_frex/sg;
  $CondReg ||= $s;
}
# s/_ISSUING_DIST_POINT_it/_ISSUING_DIST_POINT_it/g
{ $s = s /_ISSUING_DIST_POINT_it/_ISSUING_DIST_POINT_it/sg;
  $CondReg ||= $s;
}
# s/_CRL_DIST_POINTS_free/_CRL_DIST_POINTS_frex/g
{ $s = s /_CRL_DIST_POINTS_free/_CRL_DIST_POINTS_frex/sg;
  $CondReg ||= $s;
}
# s/_CRL_DIST_POINTS_it/_CRL_DIST_POINTS_ix/g
{ $s = s /_CRL_DIST_POINTS_it/_CRL_DIST_POINTS_ix/sg;
  $CondReg ||= $s;
}
# s/_DIST_POINT_free/_DIST_POINT_frex/g
{ $s = s /_DIST_POINT_free/_DIST_POINT_frex/sg;
  $CondReg ||= $s;
}
# s/_DIST_POINT_it/_DIST_POINT_ix/g
{ $s = s /_DIST_POINT_it/_DIST_POINT_ix/sg;
  $CondReg ||= $s;
}
# s/_DIST_POINT_NAME_it/_DIST_POINT_NAME_ix/g
{ $s = s /_DIST_POINT_NAME_it/_DIST_POINT_NAME_ix/sg;
  $CondReg ||= $s;
}
# s/_AUTHORITY_INFO_ACCE/_AUTHORITY_INFO_ACix/g
{ $s = s /_AUTHORITY_INFO_ACCE/_AUTHORITY_INFO_ACix/sg;
  $CondReg ||= $s;
}
# s/_AUTHORITY_KEYID_free/_AUTHORITY_KEYID_frex/g
{ $s = s /_AUTHORITY_KEYID_free/_AUTHORITY_KEYID_frex/sg;
  $CondReg ||= $s;
}
# s/_AUTHORITY_KEYID_it/_AUTHORITY_KEYID_ix/g
{ $s = s /_AUTHORITY_KEYID_it/_AUTHORITY_KEYID_ix/sg;
  $CondReg ||= $s;
}
# s/_AUTHORITY_KEYID_new/_AUTHORITY_KEYID_nex/g
{ $s = s /_AUTHORITY_KEYID_new/_AUTHORITY_KEYID_nex/sg;
  $CondReg ||= $s;
}
# s/_PROXY_CERT_INFO_EXTENSION_free/_PROXY_CERT_INFO_EXTENSION_frex/g
{ $s = s /_PROXY_CERT_INFO_EXTENSION_free/_PROXY_CERT_INFO_EXTENSION_frex/sg;
  $CondReg ||= $s;
}
# s/_PROXY_CERT_INFO_EXTENSION_it/_PROXY_CERT_INFO_EXTENSION_ix/g
{ $s = s /_PROXY_CERT_INFO_EXTENSION_it/_PROXY_CERT_INFO_EXTENSION_ix/sg;
  $CondReg ||= $s;
}
# s/_PROXY_CERT_INFO_EXTENSION_new/_PROXY_CERT_INFO_EXTENSION_nex/g
{ $s = s /_PROXY_CERT_INFO_EXTENSION_new/_PROXY_CERT_INFO_EXTENSION_nex/sg;
  $CondReg ||= $s;
}
# s/_level_find_node/_level_find_noxx/g
{ $s = s /_level_find_node/_level_find_noxx/sg;
  $CondReg ||= $s;
}
# s/_tree_find_sk/_tree_find_xx/g
{ $s = s /_tree_find_sk/_tree_find_xx/sg;
  $CondReg ||= $s;
}
# s/_level_add_node/_level_add_nodx/g
{ $s = s /_level_add_node/_level_add_nodx/sg;
  $CondReg ||= $s;
}
# s/_CMS_SignerInfo_get0_algs/_CMS_SignerInfo_get0_alxx/g
{ $s = s /_CMS_SignerInfo_get0_algs/_CMS_SignerInfo_get0_alxx/sg;
  $CondReg ||= $s;
}
# s/_CMS_RecipientInfo_ktri_get0_algs/_CMS_RecipientInfo_ktri_get0_axxl/g
{ $s = s /_CMS_RecipientInfo_ktri_get0_algs/_CMS_RecipientInfo_ktri_get0_axxl/sg;
  $CondReg ||= $s;
}
# s/_TLSv1_server_method/_TLSv1_server_methxx/g
{ $s = s /_TLSv1_server_method/_TLSv1_server_methxx/sg;
  $CondReg ||= $s;
}
# s/_TLSv1_client_method/_TLSv1_client_methxx/g
{ $s = s /_TLSv1_client_method/_TLSv1_client_methxx/sg;
  $CondReg ||= $s;
}
# s/_TLSv1_enc_data/_TLSv1_enc_datx/g
{ $s = s /_TLSv1_enc_data/_TLSv1_enc_datx/sg;
  $CondReg ||= $s;
}
# s/_DTLSv1_server_method/_DTLSv1_server_methoJ/g
{ $s = s /_DTLSv1_server_method/_DTLSv1_server_methoJ/sg;
  $CondReg ||= $s;
}
# s/_DTLSv1_client_method/_DTLSv1_client_methxx/g
{ $s = s /_DTLSv1_client_method/_DTLSv1_client_methxx/sg;
  $CondReg ||= $s;
}
# s/_DTLSv1_enc_data/_DTLSv1_enc_daxx/g
{ $s = s /_DTLSv1_enc_data/_DTLSv1_enc_daxx/sg;
  $CondReg ||= $s;
}
# s/_DIST_POINT/_XXXX_POINT/g
{ $s = s /_DIST_POINT/_XXXX_POINT/sg;
  $CondReg ||= $s;
}
# s/_TLSv1/_nanan/g
{ $s = s /_TLSv1/_nanan/sg;
  $CondReg ||= $s;
}
# s/_DTLSv1/_ianany/g
{ $s = s /_DTLSv1/_ianany/sg;
  $CondReg ||= $s;
}
# s/_SRP/_uiu/g
{ $s = s /_SRP/_uiu/sg;
  $CondReg ||= $s;
}
# s/_FIPS/_Iuiu/g
{ $s = s /_FIPS/_Iuiu/sg;
  $CondReg ||= $s;
}
# s/_CMAC/_Xuiu/g
{ $s = s /_CMAC/_Xuiu/sg;
  $CondReg ||= $s;
}
# s/_cmac/_tuiu/g
{ $s = s /_cmac/_tuiu/sg;
  $CondReg ||= $s;
}
# s/_ECPARAMETERS/_ECPARAnnTERS/g
{ $s = s /_ECPARAMETERS/_ECPARAnnTERS/sg;
  $CondReg ||= $s;
}
# s/_STACK_version/_STACK_nnnnion/g
{ $s = s /_STACK_version/_STACK_nnnnion/sg;
  $CondReg ||= $s;
}
# s/_NETSCAPE/_nnnnCAPE/g
{ $s = s /_NETSCAPE/_nnnnCAPE/sg;
  $CondReg ||= $s;
}
# s/_RMD160/_abc160/g
{ $s = s /_RMD160/_abc160/sg;
  $CondReg ||= $s;
}
# s/_OSSL_libdes/_NNNN_libdex/g
{ $s = s /_OSSL_libdes/_NNNN_libdex/sg;
  $CondReg ||= $s;
}
# s/_DIRECTORYSTRING/_DIRECTORYxxRING/g
{ $s = s /_DIRECTORYSTRING/_DIRECTORYxxRING/sg;
  $CondReg ||= $s;
}
# s/_default_pctx/_default_xxxx/g
{ $s = s /_default_pctx/_default_xxxx/sg;
  $CondReg ||= $s;
}
# s/_EDIPARTYNAME/_EDIPARTYNxxx/g
{ $s = s /_EDIPARTYNAME/_EDIPARTYNxxx/sg;
  $CondReg ||= $s;
}
# s/_OTHERNAME/_OTHERxxxx/g
{ $s = s /_OTHERNAME/_OTHERxxxx/sg;
  $CondReg ||= $s;
}
# s/_SXNET_it/_SXNET_xx/g
{ $s = s /_SXNET_it/_SXNET_xx/sg;
  $CondReg ||= $s;
}
# s/SRIO/CxIO/g
{ $s = s /SRIO/CxIO/sg;
  $CondReg ||= $s;
}
# s/SRRun/CxRun/g
{ $s = s /SRRun/CxRun/sg;
  $CondReg ||= $s;
}
# s/SRWebSocket/CxWebSocket/g
{ $s = s /SRWebSocket/CxWebSocket/sg;
  $CondReg ||= $s;
}
# s/_b64_ntop/_b64_ntox/g
{ $s = s /_b64_ntop/_b64_ntox/sg;
  $CondReg ||= $s;
}
# # New 11/12/13
# s/_SXNET_add_id_asc/_SXNET_add_id_axx/g
{ $s = s /_SXNET_add_id_asc/_SXNET_add_id_axx/sg;
  $CondReg ||= $s;
}
# s/_POLICYQUALINFO_new/_POLICYQUALINFO_nxx/g
{ $s = s /_POLICYQUALINFO_new/_POLICYQUALINFO_nxx/sg;
  $CondReg ||= $s;
}
# s/_p_CSwift_AcquireAccContext/_p_CSwift_AcquireAccContxxx/g 
{ $s = s /_p_CSwift_AcquireAccContext/_p_CSwift_AcquireAccContxxx/sg;
  $CondReg ||= $s;
}
# s/_p_CSwift_ReleaseAccContext/_p_CSwift_ReleaseAccContxxx/g 
{ $s = s /_p_CSwift_ReleaseAccContext/_p_CSwift_ReleaseAccContxxx/sg;
  $CondReg ||= $s;
}
# s/_p_CSwift_SimpleRequest/_p_CSwift_SimpleRequxxx/g 
{ $s = s /_p_CSwift_SimpleRequest/_p_CSwift_SimpleRequxxx/sg;
  $CondReg ||= $s;
}
# s/_p_CSwift_AttachKeyParam/_p_CSwift_AttachKeyPaxxx/g 
{ $s = s /_p_CSwift_AttachKeyParam/_p_CSwift_AttachKeyPaxxx/sg;
  $CondReg ||= $s;
}
# s/_pkey_gost2001_derive/_pkey_gost2001_derxxx/g 
{ $s = s /_pkey_gost2001_derive/_pkey_gost2001_derxxx/sg;
  $CondReg ||= $s;
}
# s/_pkey_GOST01cp_encrypt/_pkey_GOST01cp_encrxxx/g 
{ $s = s /_pkey_GOST01cp_encrypt/_pkey_GOST01cp_encrxxx/sg;
  $CondReg ||= $s;
}
# s/_pkey_GOST01cp_decrypt/_pkey_GOST01cp_decrxxx/g 
{ $s = s /_pkey_GOST01cp_decrypt/_pkey_GOST01cp_decrxxx/sg;
  $CondReg ||= $s;
}
# s/_fill_GOST2001_params/_fill_GOST2001_parxxx/g 
{ $s = s /_fill_GOST2001_params/_fill_GOST2001_parxxx/sg;
  $CondReg ||= $s;
}
# s/_gost2001_do_sign/_gost2001_do_sxxx/g 
{ $s = s /_gost2001_do_sign/_gost2001_do_sxxx/sg;
  $CondReg ||= $s;
}
# s/_gost2001_do_verify/_gost2001_do_verxxx/g 
{ $s = s /_gost2001_do_verify/_gost2001_do_verxxx/sg;
  $CondReg ||= $s;
}
# s/_gost2001_compute_public/_gost2001_compute_pubxxx/g 
{ $s = s /_gost2001_compute_public/_gost2001_compute_pubxxx/sg;
  $CondReg ||= $s;
}
# s/_gost2001_keygen/_gost2001_keyxxx/g 
{ $s = s /_gost2001_keygen/_gost2001_keyxxx/sg;
  $CondReg ||= $s;
}
# s/_gostcrypt/_gostcrxxx/g 
{ $s = s /_gostcrypt/_gostcrxxx/sg;
  $CondReg ||= $s;
}
# s/_gostdecrypt/_gostdecrxxx/g 
{ $s = s /_gostdecrypt/_gostdecrxxx/sg;
  $CondReg ||= $s;
}
# s/_gost_enc/_gost_exx/g 
{ $s = s /_gost_enc/_gost_exx/sg;
  $CondReg ||= $s;
}
# s/_gost_dec/_gost_dxx/g 
{ $s = s /_gost_dec/_gost_dxx/sg;
  $CondReg ||= $s;
}
# s/_gost_enc_cfb/_gost_enc_cxx/g 
{ $s = s /_gost_enc_cfb/_gost_enc_cxx/sg;
  $CondReg ||= $s;
}
# s/_gost_enc_with_key/_gost_enc_with_kxx/g 
{ $s = s /_gost_enc_with_key/_gost_enc_with_kxx/sg;
  $CondReg ||= $s;
}
# s/_gost_key/_gost_kxx/g 
{ $s = s /_gost_key/_gost_kxx/sg;
  $CondReg ||= $s;
}
# s/_gost_init/_gost_inxx/g 
{ $s = s /_gost_init/_gost_inxx/sg;
  $CondReg ||= $s;
}
# s/_GostR3411_94_TestParamSet/_GostR3411_94_TestParamSxx/g 
{ $s = s /_GostR3411_94_TestParamSet/_GostR3411_94_TestParamSxx/sg;
  $CondReg ||= $s;
}
# s/_gost_destroy/_gost_destxxx/g  
{ $s = s /_gost_destroy/_gost_destxxx/sg;
  $CondReg ||= $s;
}
# s/_mac_block/_mac_blxxx/g 
{ $s = s /_mac_block/_mac_blxxx/sg;
  $CondReg ||= $s;
}
# s/_get_mac/_get_mxx/g 
{ $s = s /_get_mac/_get_mxx/sg;
  $CondReg ||= $s;
}
# s/_gost_mac_iv/_gost_mac_ix/g 
{ $s = s /_gost_mac_iv/_gost_mac_ix/sg;
  $CondReg ||= $s;
}
# s/_cryptopro_key_meshing/_cryptopro_key_meshxxx/g 
{ $s = s /_cryptopro_key_meshing/_cryptopro_key_meshxxx/sg;
  $CondReg ||= $s;
}
# s/_CryptoProKeyMeshingKey/_CryptoProKeyMeshingKxx/g 
{ $s = s /_CryptoProKeyMeshingKey/_CryptoProKeyMeshingKxx/sg;
  $CondReg ||= $s;
}
# s/_GostR3411_94_CryptoProParamSet/_GostR3411_94_CryptoProParamSxx/g 
{ $s = s /_GostR3411_94_CryptoProParamSet/_GostR3411_94_CryptoProParamSxx/sg;
  $CondReg ||= $s;
}
# s/_Gost28147_TestParamSet/_Gost28147_TestParamSxx/g 
{ $s = s /_Gost28147_TestParamSet/_Gost28147_TestParamSxx/sg;
  $CondReg ||= $s;
}
# s/_Gost28147_CryptoProParamSetA/_Gost28147_CryptoProParamSxxa/g 
{ $s = s /_Gost28147_CryptoProParamSetA/_Gost28147_CryptoProParamSxxa/sg;
  $CondReg ||= $s;
}
# s/_Gost28147_CryptoProParamSetB/_Gost28147_CryptoProParamSxxb/g 
{ $s = s /_Gost28147_CryptoProParamSetB/_Gost28147_CryptoProParamSxxb/sg;
  $CondReg ||= $s;
}
# s/_Gost28147_CryptoProParamSetC/_Gost28147_CryptoProParamSxxc/g 
{ $s = s /_Gost28147_CryptoProParamSetC/_Gost28147_CryptoProParamSxxc/sg;
  $CondReg ||= $s;
}
# s/_Gost28147_CryptoProParamSetD/_Gost28147_CryptoProParamSxxd/g 
{ $s = s /_Gost28147_CryptoProParamSetD/_Gost28147_CryptoProParamSxxd/sg;
  $CondReg ||= $s;
}
# s/_pkey_gost94_derive/_pkey_gost94_derxxx/g 
{ $s = s /_pkey_gost94_derive/_pkey_gost94_derxxx/sg;
  $CondReg ||= $s;
}
# s/_pkey_GOST94cp_encrypt/_pkey_GOST94cp_encrxxx/g 
{ $s = s /_pkey_GOST94cp_encrypt/_pkey_GOST94cp_encrxxx/sg;
  $CondReg ||= $s;
}
# s/_pkey_GOST94cp_decrypt/_pkey_GOST94cp_decrxxx/g 
{ $s = s /_pkey_GOST94cp_decrypt/_pkey_GOST94cp_decrxxx/sg;
  $CondReg ||= $s;
}
# s/_gost94_nid_by_params_/gost94_nid_by_parxxx/g 
{ $s = s /_gost94_nid_by_params_/gost94_nid_by_parxxx/sg;
  $CondReg ||= $s;
}
# s/_gost_get0_priv_key/_gost_get0_priv_kxx/g 
{ $s = s /_gost_get0_priv_key/_gost_get0_priv_kxx/sg;
  $CondReg ||= $s;
}
# s/_register_ameth_gost/_register_ameth_goxx/g 
{ $s = s /_register_ameth_gost/_register_ameth_goxx/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_TRANSPORT_it/_GOST_KEY_TRANSPORT_xx/g 
{ $s = s /_GOST_KEY_TRANSPORT_it/_GOST_KEY_TRANSPORT_xx/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_TRANSPORT_new/_GOST_KEY_TRANSPORT_nxx/g 
{ $s = s /_GOST_KEY_TRANSPORT_new/_GOST_KEY_TRANSPORT_nxx/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_TRANSPORT_free/_GOST_KEY_TRANSPORT_frxx/g 
{ $s = s /_GOST_KEY_TRANSPORT_free/_GOST_KEY_TRANSPORT_frxx/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_INFO_it/_GOST_KEY_INFO_ix/g 
{ $s = s /_GOST_KEY_INFO_it/_GOST_KEY_INFO_ix/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_AGREEMENT_INFO_it/_GOST_KEY_AGREEMENT_INFO_ix/g 
{ $s = s /_GOST_KEY_AGREEMENT_INFO_it/_GOST_KEY_AGREEMENT_INFO_ix/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_PARAMS_it/_GOST_KEY_PARAMS_ix/g 
{ $s = s /_GOST_KEY_PARAMS_it/_GOST_KEY_PARAMS_ix/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_PARAMS_new/_GOST_KEY_PARAMS_nxx/g 
{ $s = s /_GOST_KEY_PARAMS_new/_GOST_KEY_PARAMS_nxx/sg;
  $CondReg ||= $s;
}
# s/_GOST_KEY_PARAMS_free/_GOST_KEY_PARAMS_frxx/g 
{ $s = s /_GOST_KEY_PARAMS_free/_GOST_KEY_PARAMS_frxx/sg;
  $CondReg ||= $s;
}
# s/_GOST_CIPHER_PARAMS_it/_GOST_CIPHER_PARAMS_ix/g 
{ $s = s /_GOST_CIPHER_PARAMS_it/_GOST_CIPHER_PARAMS_ix/sg;
  $CondReg ||= $s;
}
# s/_GOST_CIPHER_PARAMS_new/_GOST_CIPHER_PARAMS_nxx/g 
{ $s = s /_GOST_CIPHER_PARAMS_new/_GOST_CIPHER_PARAMS_nxx/sg;
  $CondReg ||= $s;
}
# s/_GOST_CIPHER_PARAMS_free/_GOST_CIPHER_PARAMS_frxx/g 
{ $s = s /_GOST_CIPHER_PARAMS_free/_GOST_CIPHER_PARAMS_frxx/sg;
  $CondReg ||= $s;
}
# s/_get_encryption_params/_get_encryption_paraxx/g 
{ $s = s /_get_encryption_params/_get_encryption_paraxx/sg;
  $CondReg ||= $s;
}
# s/_gost_cipher_list/_gost_cipher_lixx/g 
{ $s = s /_gost_cipher_list/_gost_cipher_lixx/sg;
  $CondReg ||= $s;
}
# s/_cipher_gost/_cipher_goxx/g 
{ $s = s /_cipher_gost/_cipher_goxx/sg;
  $CondReg ||= $s;
}
# s/_cipher_gost_cpacnt/_cipher_gost_cpaxxx/g 
{ $s = s /_cipher_gost_cpacnt/_cipher_gost_cpaxxx/sg;
  $CondReg ||= $s;
}
# s/_imit_gost_cpa/_imit_gost_cxx/g 
{ $s = s /_imit_gost_cpa/_imit_gost_cxx/sg;
  $CondReg ||= $s;
}
# s/_gost_param_free/_gost_param_frxx/g 
{ $s = s /_gost_param_free/_gost_param_frxx/sg;
  $CondReg ||= $s;
}
# s/_gost_control_func/_gost_control_fxxx/g 
{ $s = s /_gost_control_func/_gost_control_fxxx/sg;
  $CondReg ||= $s;
}
# s/_gost_cmds/_gost_cmxx/g 
{ $s = s /_gost_cmds/_gost_cmxx/sg;
  $CondReg ||= $s;
}
# s/_init_gost_hash_ctx/_init_gost_hash_cxx/g  
{ $s = s /_init_gost_hash_ctx/_init_gost_hash_cxx/sg;
  $CondReg ||= $s;
}
# s/_done_gost_hash_ctx/_done_gost_hash_cxx/g 
{ $s = s /_done_gost_hash_ctx/_done_gost_hash_cxx/sg;
  $CondReg ||= $s;
}
# s/_start_hash/_start_haxx/g 
{ $s = s /_start_hash/_start_haxx/sg;
  $CondReg ||= $s;
}
# s/_hash_block/_hash_bloxx/g 
{ $s = s /_hash_block/_hash_bloxx/sg;
  $CondReg ||= $s;
}
# s/_finish_hash/_finish_haxx/g 
{ $s = s /_finish_hash/_finish_haxx/sg;
  $CondReg ||= $s;
}
# s/_keyDiversifyCryptoPro/_keyDiversifyCryptoPxx/g 
{ $s = s /_keyDiversifyCryptoPro/_keyDiversifyCryptoPxx/sg;
  $CondReg ||= $s;
}
# s/_keyWrapCryptoPro/_keyWrapCryptoPxx/g 
{ $s = s /_keyWrapCryptoPro/_keyWrapCryptoPxx/sg;
  $CondReg ||= $s;
}
# s/_keyUnwrapCryptoPro/_keyUnwrapCryptoPxx/g 
{ $s = s /_keyUnwrapCryptoPro/_keyUnwrapCryptoPxx/sg;
  $CondReg ||= $s;
}
# s/_digest_gost/_digest_goxx/g 
{ $s = s /_digest_gost/_digest_goxx/sg;
  $CondReg ||= $s;
}
# s/_R3410_paramset/_R3410_paramsxx/g 
{ $s = s /_R3410_paramset/_R3410_paramsxx/sg;
  $CondReg ||= $s;
}
# s/_R3410_2001_paramset/_R3410_2001_paramsxx/g 
{ $s = s /_R3410_2001_paramset/_R3410_2001_paramsxx/sg;
  $CondReg ||= $s;
}
# s/_register_pmeth_gost/_register_pmeth_goxx/g 
{ $s = s /_register_pmeth_gost/_register_pmeth_goxx/sg;
  $CondReg ||= $s;
}
# s/_gost_do_sign/_gost_do_sixx/g 
{ $s = s /_gost_do_sign/_gost_do_sixx/sg;
  $CondReg ||= $s;
}
# s/_hashsum2bn/_hashsum2xx/g 
{ $s = s /_hashsum2bn/_hashsum2xx/sg;
  $CondReg ||= $s;
}
# s/_pack_sign_cp/_pack_sign_cx/g 
{ $s = s /_pack_sign_cp/_pack_sign_cx/sg;
  $CondReg ||= $s;
}
# s/_store_bignum/_store_bigxxx/g 
{ $s = s /_store_bignum/_store_bigxxx/sg;
  $CondReg ||= $s;
}
# s/_gost_do_verify/_gost_do_verxxx/g 
{ $s = s /_gost_do_verify/_gost_do_verxxx/sg;
  $CondReg ||= $s;
}
# s/_gost94_compute_public/_gost94_compute_pubxxx/g 
{ $s = s /_gost94_compute_public/_gost94_compute_pubxxx/sg;
  $CondReg ||= $s;
}
# s/_fill_GOST94_params/_fill_GOST94_parxxx/g 
{ $s = s /_fill_GOST94_params/_fill_GOST94_parxxx/sg;
  $CondReg ||= $s;
}
# s/_gost_sign_keygen/_gost_sign_keyxxx/g 
{ $s = s /_gost_sign_keygen/_gost_sign_keyxxx/sg;
  $CondReg ||= $s;
}
# s/_unpack_cp_signature/_unpack_cp_signatxxx/g 
{ $s = s /_unpack_cp_signature/_unpack_cp_signatxxx/sg;
  $CondReg ||= $s;
}
# s/_getbnfrombuf/_getbnfromxxx/g 
{ $s = s /_getbnfrombuf/_getbnfromxxx/sg;
  $CondReg ||= $s;
}
# s/_SXNETID_new/_SXNETID_nxx/g
{ $s = s /_SXNETID_new/_SXNETID_nxx/sg;
  $CondReg ||= $s;
}
# s/_SXNETID_free/_SXNETID_frxx/g
{ $s = s /_SXNETID_free/_SXNETID_frxx/sg;
  $CondReg ||= $s;
}
# s/_SXNET_new/_SXNET_nxx/g
{ $s = s /_SXNET_new/_SXNET_nxx/sg;
  $CondReg ||= $s;
}
# s/_SXNET_free/_SXNET_frxx/g
{ $s = s /_SXNET_free/_SXNET_frxx/sg;
  $CondReg ||= $s;
}
# s/_SXNET_get_id_INTEGER/_SXNET_get_id_INTEGXX/g
{ $s = s /_SXNET_get_id_INTEGER/_SXNET_get_id_INTEGXX/sg;
  $CondReg ||= $s;
}
# s/_gost94_nid_by_params/_gost94_nid_by_paraxx/g
{ $s = s /_gost94_nid_by_params/_gost94_nid_by_paraxx/sg;
  $CondReg ||= $s;
}
# # WEBRTC-5870
# s/_CAMELLIA_version/_CAMELLIZ_version/g
{ $s = s /_CAMELLIA_version/_CAMELLIZ_version/sg;
  $CondReg ||= $s;
}
# s/_CAST_/_ZAST_/g
{ $s = s /_CAST_/_ZAST_/sg;
  $CondReg ||= $s;
}
# s/_CERTIFICATEPOLICIES_/_ZERTIFICATEPOLICIES_/g
{ $s = s /_CERTIFICATEPOLICIES_/_ZERTIFICATEPOLICIES_/sg;
  $CondReg ||= $s;
}
# s/_CMS_/_CZS_/g
{ $s = s /_CMS_/_CZS_/sg;
  $CondReg ||= $s;
}
# s/_ECParameters_/_EZParameters_/g
{ $s = s /_ECParameters_/_EZParameters_/sg;
  $CondReg ||= $s;
}
# s/_ESS_/_ZSS_/g
{ $s = s /_ESS_/_ZSS_/sg;
  $CondReg ||= $s;
}
# s/_EXTENDED_KEY_USAGE_/_ZXTENDED_KEY_USAGE_/g
{ $s = s /_EXTENDED_KEY_USAGE_/_ZXTENDED_KEY_USAGE_/sg;
  $CondReg ||= $s;
}
# s/_GOST_/_GPPT_/g
{ $s = s /_GOST_/_GPPT_/sg;
  $CondReg ||= $s;
}
# s/_IDEA_version/_IDEA_versioz/g
{ $s = s /_IDEA_version/_IDEA_versioz/sg;
  $CondReg ||= $s;
}
# s/_KRB5_/_KRB8_/g
{ $s = s /_KRB5_/_KRB8_/sg;
  $CondReg ||= $s;
}
# s/_SEED_/_5EED_/g
{ $s = s /_SEED_/_5EED_/sg;
  $CondReg ||= $s;
}
# s/_SMIME_/_5MIME_/g
{ $s = s /_SMIME_/_5MIME_/sg;
  $CondReg ||= $s;
}
# s/_SXNET_/_5XNET_/g
{ $s = s /_SXNET_/_5XNET_/sg;
  $CondReg ||= $s;
}
# s/_TS_/_T5_/g
{ $s = s /_TS_/_T5_/sg;
  $CondReg ||= $s;
}
# s/_TXT_DB_/_6XT_DB_/g
{ $s = s /_TXT_DB_/_6XT_DB_/sg;
  $CondReg ||= $s;
}
# s/__ossl_/__o55l_/g
{ $s = s /__ossl_/__o55l_/sg;
  $CondReg ||= $s;
}
# s/_b2i_/_b8i_/g
{ $s = s /_b2i_/_b8i_/sg;
  $CondReg ||= $s;
}
# s/_cms_/_cm5_/g
{ $s = s /_cms_/_cm5_/sg;
  $CondReg ||= $s;
}
# s/_fcrypt_body/_fcrypt_b0dy/g
{ $s = s /_fcrypt_body/_fcrypt_b0dy/sg;
  $CondReg ||= $s;
}
# s/_get_rfc2409_prime_/_get_rfc2409_pr1me_/g
{ $s = s /_get_rfc2409_prime_/_get_rfc2409_pr1me_/sg;
  $CondReg ||= $s;
}
# s/_get_rfc3526_prime_/_get_rfc3526_pr1me_/g
{ $s = s /_get_rfc3526_prime_/_get_rfc3526_pr1me_/sg;
  $CondReg ||= $s;
}
# s/_gost_get_key/_gost_get_k3y/g
{ $s = s /_gost_get_key/_gost_get_k3y/sg;
  $CondReg ||= $s;
}
# s/_gost_mac/_go5t_mac/g
{ $s = s /_gost_mac/_go5t_mac/sg;
  $CondReg ||= $s;
}
# s/_gost_set_default_param/_go5t_set_default_param/g
{ $s = s /_gost_set_default_param/_go5t_set_default_param/sg;
  $CondReg ||= $s;
}
# s/_i2b_/_i7b_/g
{ $s = s /_i2b_/_i7b_/sg;
  $CondReg ||= $s;
}
# s/_idea_/_id3a_/g
{ $s = s /_idea_/_id3a_/sg;
  $CondReg ||= $s;
}
# s/_SHA/_SZA/g
{ $s = s /_SHA/_SZA/sg;
  $CondReg ||= $s;
}
# s/_SHA1/_SZZ1/g
{ $s = s /_SHA1/_SZZ1/sg;
  $CondReg ||= $s;
}
# s/_SHA224/_SZA224/g
{ $s = s /_SHA224/_SZA224/sg;
  $CondReg ||= $s;
}
# s/_SHA224/_SZA224/g
{ $s = s /_SHA224/_SZA224/sg;
  $CondReg ||= $s;
}
# s/_SHA256/_SZA256/g
{ $s = s /_SHA256/_SZA256/sg;
  $CondReg ||= $s;
}
# s/_SHA384/_SZA384/g
{ $s = s /_SHA384/_SZA384/sg;
  $CondReg ||= $s;
}
# s/_SHA512/_SZA512/g
{ $s = s /_SHA512/_SZA512/sg;
  $CondReg ||= $s;
}
# s/__des_crypt/__d3s_crypt/g
{ $s = s /__des_crypt/__d3s_crypt/sg;
  $CondReg ||= $s;
}
# #Replace saved strings
# s/CALL_ZONFIG/CALL_CONFIG/g
{ $s = s /CALL_ZONFIG/CALL_CONFIG/sg;
  $CondReg ||= $s;
}
# s/_CC_NWA1/_CC_SHA1/g
{ $s = s /_CC_NWA1/_CC_SHA1/sg;
  $CondReg ||= $s;
}
# s/_ZONVERSATION/_CONVERSATION/g
{ $s = s /_ZONVERSATION/_CONVERSATION/sg;
  $CondReg ||= $s;
}
# s/_ZONFLICT/_CONFLICT/g
{ $s = s /_ZONFLICT/_CONFLICT/sg;
  $CondReg ||= $s;
}
# s/_Ios_Nickmode/_Ios_Openmode/g
{ $s = s /_Ios_Nickmode/_Ios_Openmode/sg;
  $CondReg ||= $s;
}
# s/abcd_cdef/HMAC_SHA1/g
{ $s = s /abcd_cdef/HMAC_SHA1/sg;
  $CondReg ||= $s;
}
# s/lhi_kg/h_main/g
{ $s = s /lhi_kg/h_main/sg;
  $CondReg ||= $s;
}
EOS:    if( $doPrint ){
            print $_, "\n";
        } else {
	    $doPrint = $doAutoPrint;
	}
        printQ() if @Q;
    }

    exit( 0 );
}
Run();

# openARGV: open 1st input file
#
sub openARGV(){
    unshift( @ARGV, '-' ) unless @ARGV;
    my $file = shift( @ARGV );
    open( ARG, "<$file" )
    || die( "$0: can't open $file for reading ($!)\n" );
    $isEOF = 0;
}

# getsARGV: Read another input line into argument (default: $_).
#           Move on to next input file, and reset EOF flag $isEOF.
sub getsARGV(;\$){
    my $argref = @_ ? shift() : \$_; 
    while( $isEOF || ! defined( $$argref = <ARG> ) ){
	close( ARG );
	return 0 unless @ARGV;
	my $file = shift( @ARGV );
	open( ARG, "<$file" )
	|| die( "$0: can't open $file for reading ($!)\n" );
	$isEOF = 0;
    }
    1;
}

# eofARGV: end-of-file test
#
sub eofARGV(){
    return @ARGV == 0 && ( $isEOF = eof( ARG ) );
}

# makeHandle: Generates another file handle for some file (given by its path)
#             to be written due to a w command or an s command's w flag.
sub makeHandle($){
    my( $path ) = @_;
    my $handle;
    if( ! exists( $wFiles{$path} ) || $wFiles{$path} eq '' ){
        $handle = $wFiles{$path} = gensym();
	if( $doOpenWrite ){
	    if( ! open( $handle, ">$path" ) ){
		die( "$0: can't open $path for writing: ($!)\n" );
	    }
	}
    } else {
        $handle = $wFiles{$path};
    }
    return $handle;
}

# printQ: Print queued output which is either a string or a reference
#         to a pathname.
sub printQ(){
    for my $q ( @Q ){
	if( ref( $q ) ){
            # flush open w files so that reading this file gets it all
	    if( exists( $wFiles{$$q} ) && $wFiles{$$q} ne '' ){
		open( $wFiles{$$q}, ">>$$q" );
	    }
            # copy file to stdout: slow, but safe
	    if( open( RF, "<$$q" ) ){
		while( defined( my $line = <RF> ) ){
		    print $line;
		}
		close( RF );
	    }
	} else {
	    print $q;
	}
    }
    undef( @Q );
}

