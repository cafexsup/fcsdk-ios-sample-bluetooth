//
//  UCClientTabbedViewViewController.h
//  ImPresenceSample
//
//  Created by Tom Greenwood on 29/01/2013.
//

#import <UIKit/UIKit.h>
#import <ACBClientSDK/ACBUC.h>

@protocol UCConsumer <NSObject>
    @property ACBUC *uc;
@end

@protocol UCSessionHandler <UCConsumer>
    @property NSString *configuration;
    @property NSString *server;
@end

@interface UCClientTabbedViewController : UITabBarController
    @property ACBUC *uc;
    @property NSString *server;
    @property NSString *configuration;
@end