//
//  AEDViewController.m
//
//  Created by swest on 07/03/2013.
//

#import "AEDViewController.h"

@interface AEDViewController ()

@end

@implementation AEDViewController

@synthesize uc, currentTopic;

NSMutableArray *topicList;
NSMutableArray *logList;
NSString *untickedBoxStr = @"\u2610"; //unicode character representing empty box
NSString *tickedBoxStr = @"\u2611"; //unicode character representing checked box

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)connectToTopic:(id)sender
{
    //create a Topic and connect to it
    // we will be automatically subscribed to
    // notifications for that topic
    int expiry = [_timeout.text intValue];
    currentTopic = [uc.aed createTopicWithName:_topicName.text expiryTime:expiry delegate:self];
}

- (IBAction)disconnectTopic:(id)sender
{
     if(currentTopic.connected)
     {
         if([_deleteTopic.text isEqualToString:tickedBoxStr])
         {
             [currentTopic disconnectWithDeleteFlag:TRUE];
         }
         else
         {
             [currentTopic disconnectWithDeleteFlag:FALSE];
         }

         [topicList removeObject:currentTopic.name];
         [self.topicView reloadData];
         NSString *msg = [NSString stringWithFormat:@"Topic '%@' disconnected.", currentTopic.name];
         [self logMessage:msg];
     }
     else
     {
         NSString *msg = [NSString stringWithFormat:@"Topic '%@' already disconnected.", currentTopic.name];
         [self logMessage:msg];
     }
}

- (IBAction)publishData:(id)sender
{
    [currentTopic submitDataWithKey:_dataKey.text value:_dataValue.text];
}

- (IBAction)deleteData:(id)sender
{
    [currentTopic deleteDataWithKey:_dataKey.text];
}

- (IBAction)sendMessage:(id)sender
{
    [currentTopic sendAedMessage:_message.text];
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    if (tableView == _topicView)
    {
        rows = [topicList count];
    }
    if (tableView == _logView2)
    {
        rows = [logList count];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }

    if (tableView == _topicView)
    {
        cell.textLabel.text = [topicList objectAtIndex:indexPath.row];
    }
    if (tableView == _logView2)
    {
        cell.textLabel.text = [logList objectAtIndex:indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _topicView)
    {
        UITableViewCell *cell = [_topicView cellForRowAtIndexPath:indexPath];
        currentTopic = [uc.aed createTopicWithName:cell.textLabel.text delegate:self];
        NSString *msg = [NSString stringWithFormat:@"Current topic is '%@'.", currentTopic.name];
        [self logMessage:msg];
        _topicName.text = currentTopic.name;
    }
}

- (void)setUc:(ACBUC *)theUC
{
	uc = theUC;
}

#pragma mark Topic callbacks

- (void)topic:(ACBTopic *)topic didConnectWithData:(NSDictionary *)data
//this will tell us which topic we're connected to as well as give us
//all the topic data (up to the point where we get connected)
{
    //Aquire all information about topic and it's data
    [topicList addObject:topic.name];
    [self.topicView reloadData];
    currentTopic = topic;
    NSString* topicExpiry = [data objectForKey:@"timeout"];

    //Print out successfull connection
    NSString *msg = [NSString stringWithFormat:@"Topic '%@' connected succesfully - expiry at %@ minutes.", currentTopic.name, topicExpiry];
    [self logMessage:msg];
    msg = [NSString stringWithFormat:@"Current topic is '%@'. Topic Data:", currentTopic.name];
    [self logMessage:msg];

    // topic data is an array containing all our key/value pairs.
    NSArray *topicData = [data objectForKey:@"data"];
    if([topicData count] > 0)
    {
        //we can show our users the data in the topic as follows
        for(int i = 0; i < [topicData count] ; i++)
        {
            NSString *keyField = [[topicData objectAtIndex:i] valueForKey:@"key"];
            NSString *valueField = [[topicData objectAtIndex:i] valueForKey:@"value"];
            [self logMessage:[NSString stringWithFormat:@"Key:'%@' Value:'%@'",keyField,valueField]];
        }
    }
}

- (void)topicDidDelete:(ACBTopic *)topic
{
    NSString *msg = [NSString stringWithFormat:@"Topic '%@' has been deleted.", topic.name];
    [self logMessage:msg];
    [topicList removeObject:topic.name];
    [self.topicView reloadData];
}

- (void)topic:(ACBTopic *)topic didDeleteWithMessage:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"%@ for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didSubmitWithKey:(NSString *)key value:(NSString *)value version:(int)version
{
    NSString *msg = [NSString stringWithFormat:@"Data with k: '%@' v: '%@' in topic '%@' submitted. Version: %i", key, value, topic.name, version];

    // this is where we can check to ensure that we don't already have a newer version of the
    // data for that key, just in case there's a race condition somewhere in the system.
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didDeleteDataSuccessfullyWithKey:(NSString *)key version:(int)version
{    
    NSString *msg = [NSString stringWithFormat:@"Data with k: '%@' in topic '%@' deleted. Version: %i", key, topic.name, version];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didSendMessageSuccessfullyWithMessage:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"Sent message - %@ for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didNotConnectWithMessage:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"Connect Failed - %@ for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didNotDeleteWithMessage:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"Delete Failed - %@ for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didNotSubmitWithKey:(NSString *)key value:(NSString *)value message:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"Publish Data Failed - %@ for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didNotDeleteDataWithKey:(NSString *)key message:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"Delete Data Failed - %@ for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didNotSendMessage:(NSString *)originalMessage message:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"Send Message Failed - %@ for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

//Event Notifications
- (void)topic:(ACBTopic *)topic didUpdateWithKey:(NSString *)key value:(NSString *)value version:(int)version deleted:(BOOL)deleted
{
    NSString *msg = [NSString stringWithFormat:@"Data with for k: '%@', v: '%@' in topic '%@' updated. Version: %i", key,value, topic.name, version];
    [self logMessage:msg];
}

- (void)topic:(ACBTopic *)topic didReceiveMessage:(NSString *)message
{
    NSString *msg = [NSString stringWithFormat:@"Received Message - \"%@\" for topic '%@'.", message, topic.name];
    [self logMessage:msg];
}

#pragma mark UIViewController methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    topicList = [NSMutableArray arrayWithCapacity:30];
    logList = [NSMutableArray arrayWithCapacity:500];
    [(UIScrollView *)self.view setContentSize:CGSizeMake(768, 1024)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (CGRectContainsPoint([_deleteTopic frame], [touch locationInView:self.view]))
    {
        [self toggleDeleteTopicCheckBox];
    }
}

-(void) toggleDeleteTopicCheckBox
{
    if ([_deleteTopic.text isEqualToString:tickedBoxStr])
    {
        _deleteTopic.text = untickedBoxStr;
    }
    else
    {
        _deleteTopic.text = tickedBoxStr;
    }
}

#pragma mark utility methods

- (void) logMessage: (NSString*) msg
{
    [logList addObject:msg];
    [self.logView2 reloadData];
    NSIndexPath *idxPath = [NSIndexPath indexPathForRow:logList.count-1 inSection:0];
    [_logView2 scrollToRowAtIndexPath:idxPath
                     atScrollPosition:UITableViewScrollPositionBottom animated:TRUE];
}

@end