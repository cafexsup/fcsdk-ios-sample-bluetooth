//
//  SettingsPopoverViewController.h
//
//  Created by Leigh Hall on 19/12/2013.
//

#import <UIKit/UIKit.h>
#import "ACBVideoCaptureSetting+UI.h"

@protocol SettingsPopoverViewControllerDelegate
- (void)qualitySelectionChanged:(ACBVideoCaptureSetting*)choice;
@end

@interface SettingsPopoverViewController : UITableViewController

@property (strong) NSArray* qualityChoices; //ACBVideoCaptureSetting
@property (strong) ACBVideoCaptureSetting* currentChoice;

@property (weak) id<SettingsPopoverViewControllerDelegate> delegate;

@end
