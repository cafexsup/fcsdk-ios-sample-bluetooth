//
//  KeypadPopupViewController.m
//
//  Created by Adam Price on 28/05/2013.
//

#import "KeypadPopupViewController.h"

@interface KeypadPopupViewController ()

@end

@implementation KeypadPopupViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    CGRect rect = CGRectMake(0, 0, 318, 268);
    UIView* view = [[UIView alloc] initWithFrame:rect];
    [view setBackgroundColor:[UIColor whiteColor]];

    UIButton *button_1    = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 106, 67)];
    UIButton *button_2    = [[UIButton alloc] initWithFrame:CGRectMake(106, 0, 106, 67)];
    UIButton *button_3    = [[UIButton alloc] initWithFrame:CGRectMake(212, 0, 106, 67)];
    UIButton *button_4    = [[UIButton alloc] initWithFrame:CGRectMake(0, 67, 106, 67)];
    UIButton *button_5    = [[UIButton alloc] initWithFrame:CGRectMake(106, 67, 106, 67)];
    UIButton *button_6    = [[UIButton alloc] initWithFrame:CGRectMake(212, 67, 106, 67)];
    UIButton *button_7    = [[UIButton alloc] initWithFrame:CGRectMake(0, 134, 106, 67)];
    UIButton *button_8    = [[UIButton alloc] initWithFrame:CGRectMake(106, 134, 106, 67)];
    UIButton *button_9    = [[UIButton alloc] initWithFrame:CGRectMake(212, 134, 106, 67)];
    UIButton *button_star = [[UIButton alloc] initWithFrame:CGRectMake(0, 201, 106, 67)];
    UIButton *button_0    = [[UIButton alloc] initWithFrame:CGRectMake(106, 201, 106, 67)];
    UIButton *button_hash = [[UIButton alloc] initWithFrame:CGRectMake(212, 201, 106, 67)];

    [button_1    setBackgroundImage:[UIImage imageNamed:@"keypad_1.png"]    forState:UIControlStateNormal];
    [button_2    setBackgroundImage:[UIImage imageNamed:@"keypad_2.png"]    forState:UIControlStateNormal];
    [button_3    setBackgroundImage:[UIImage imageNamed:@"keypad_3.png"]    forState:UIControlStateNormal];
    [button_4    setBackgroundImage:[UIImage imageNamed:@"keypad_4.png"]    forState:UIControlStateNormal];
    [button_5    setBackgroundImage:[UIImage imageNamed:@"keypad_5.png"]    forState:UIControlStateNormal];
    [button_6    setBackgroundImage:[UIImage imageNamed:@"keypad_6.png"]    forState:UIControlStateNormal];
    [button_7    setBackgroundImage:[UIImage imageNamed:@"keypad_7.png"]    forState:UIControlStateNormal];
    [button_8    setBackgroundImage:[UIImage imageNamed:@"keypad_8.png"]    forState:UIControlStateNormal];
    [button_9    setBackgroundImage:[UIImage imageNamed:@"keypad_9.png"]    forState:UIControlStateNormal];
    [button_0    setBackgroundImage:[UIImage imageNamed:@"keypad_0.png"]    forState:UIControlStateNormal];
    [button_star setBackgroundImage:[UIImage imageNamed:@"keypad_star.png"] forState:UIControlStateNormal];
    [button_hash setBackgroundImage:[UIImage imageNamed:@"keypad_hash.png"] forState:UIControlStateNormal];

    [button_1    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_2    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_3    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_4    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_5    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_6    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_7    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_8    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_9    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_0    addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_star addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button_hash addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];

    [button_1    setTag:1];
    [button_2    setTag:2];
    [button_3    setTag:3];
    [button_4    setTag:4];
    [button_5    setTag:5];
    [button_6    setTag:6];
    [button_7    setTag:7];
    [button_8    setTag:8];
    [button_9    setTag:9];
    [button_0    setTag:0];
    [button_star setTag:11];
    [button_hash setTag:12];

    [view addSubview:button_1];
    [view addSubview:button_2];
    [view addSubview:button_3];
    [view addSubview:button_4];
    [view addSubview:button_5];
    [view addSubview:button_6];
    [view addSubview:button_7];
    [view addSubview:button_8];
    [view addSubview:button_9];
    [view addSubview:button_0];
    [view addSubview:button_star];
    [view addSubview:button_hash];

    [self.view addSubview:view];
}

-(void)buttonPressed:(UIButton *)sender
{
    if([self.delegate respondsToSelector:@selector(DTMFButtonPressed:)])
    {
        [self.delegate DTMFButtonPressed:sender];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end