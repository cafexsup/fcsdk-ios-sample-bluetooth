//
//  KeypadPopupViewController.h
//
//  Created by Adam Price on 28/05/2013.
//

#import <UIKit/UIKit.h>

@protocol DTMFDelegate <NSObject>
@optional
-(void)DTMFButtonPressed:(UIButton*)button;
@end

@interface KeypadPopupViewController : UIViewController
{
    id<DTMFDelegate> delegate;
}

@property (nonatomic,strong)id delegate;

@end