//
//  DialPadViewController.h
//  ImPresenceSample
//
//  Created by DevTest2 on 05/02/2013.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "UCClientTabbedViewController.h"
#import "KeypadPopupViewController.h"
#import "InCallQualityView.h"
#import "SettingsPopoverViewController.h"
#import <OpenGLES/ES2/glext.h>

@interface DialPadViewController : UIViewController<UCConsumer, ACBClientPhoneDelegate,
ACBClientCallDelegate, UIAlertViewDelegate, DTMFDelegate, SettingsPopoverViewControllerDelegate,
UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *DialNumber;
@property (weak, nonatomic) IBOutlet UIImageView *diallerBarPicture;
@property (weak, nonatomic) IBOutlet UIView *dialPad;
@property (weak, nonatomic) IBOutlet UIButton *dialpadButton;
@property (weak, nonatomic) IBOutlet UIView *localView;
@property (weak, nonatomic) IBOutlet UIView *callControls;
@property (weak, nonatomic) IBOutlet UIButton *muteButton;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *firstCallButton;
@property (weak, nonatomic) IBOutlet UIButton *secondCallButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdCallButton;
@property (weak, nonatomic) IBOutlet UIButton *fourthCallButton;
@property (weak, nonatomic) IBOutlet UILabel *firstCallButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondCallButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdCallButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourthCallButtonLabel;
@property (weak, nonatomic) IBOutlet UIView *videoView1;
@property (weak, nonatomic) IBOutlet UIView *videoView2;
@property (weak, nonatomic) IBOutlet UIView *videoView3;
@property (weak, nonatomic) IBOutlet UIView *videoView4;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) ACBClientCall *lastIncomingCall;
@property (retain) UIAlertView *incomingCallAlert;
@property (retain) UILocalNotification *incomingCallNotification;
@property (strong, nonatomic) UIPopoverController *dialpadPopover;
@property (weak, nonatomic) UIPopoverController *settingsPopover;
@property (weak, nonatomic) IBOutlet InCallQualityView *callQualityView;
@property (weak, nonatomic) IBOutlet UIView *greenBar;
@property (weak, nonatomic) IBOutlet UIView *redBar;

- (IBAction)displayDialpad:(UIButton *)sender;
- (IBAction)firstCallButtonPressed:(UIButton *)sender;
- (IBAction)secondCallButtonPressed:(UIButton *)sender;
- (IBAction)thirdCallButtonPressed:(UIButton *)sender;
- (IBAction)fourthCallButtonPressed:(UIButton *)sender;
- (IBAction)settingsButtonPressed:(UIButton*)sender;
@end
