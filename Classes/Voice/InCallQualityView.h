//
//  InCallQualityView.h
//  WebRTCClient
//
//  Created by Ceri Hughes on 21/01/2014.
//  Copyright (c) 2014 Alice Calls Bob. All rights reserved.
//

@interface InCallQualityView : UIView

@property (nonatomic) NSInteger quality;

@end
