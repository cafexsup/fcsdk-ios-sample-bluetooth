//
//  ACBVideoCaptureSetting+UI.h
//
//  Created by Leigh Hall on 19/12/2013.
//

#import <ACBClientSDK/ACBClientPhone.h>

@interface ACBVideoCaptureSetting (UI)

- (NSString*) uiString;

@end
