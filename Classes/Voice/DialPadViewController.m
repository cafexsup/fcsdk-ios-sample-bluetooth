//
//  DialPadViewController.m
//  ImPresenceSample
//
//  Created by DevTest2 on 05/02/2013.
//

#import "DialPadViewController.h"
#import "DialPadCell.h"
#import "KeypadPopupViewController.h"
#import <ACBClientSDK/ACBClientPhone.h>
#import <MediaPlayer/MediaPlayer.h>

#define IMAGE_UNMUTE @"option_unmute.png"
#define IMAGE_MUTE @"option_mute.png"
#define IMAGE_VIDEO_ENABLE @"option_video_enable.png"
#define IMAGE_VIDEO_DISABLE @"option_video_disable.png"

#define CALL_BUTTON_GREEN [UIColor colorWithRed:29.0/255.0 green:158.0/255.0 blue:9.0/255.0 alpha:1.0]
#define CALL_BUTTON_RED [UIColor colorWithRed:152.0/255.0 green:0.0/255.0 blue:12.0/255.0 alpha:1.0]
#define CALL_BUTTON_GREY [UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1.0]

@interface DialPadViewController () 

@property BOOL audio;
@property BOOL video;
@property NSString *lastNumber;
@property UIInterfaceOrientation orientation;
@property int selectedCallIndex;
@property NSArray *buttonArray;
@property NSArray *buttonLabelArray;
@property AVAudioPlayer *audioPlayer;
@property id lock;
@property ACBVideoCaptureSetting* selectedQuality;
@property (nonatomic, strong) MPVolumeView *volumeSlider;

@end

@implementation DialPadViewController
{
    ACBClientCall *calls[4];
    UIView *videoViews[4];
    bool audioEnabled[4];
    bool videoEnabled[4];
}

@synthesize uc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}

- (void)setUc:(ACBUC *)theUC
{
	uc = theUC;
	uc.phone.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRotate:) name:UIApplicationWillChangeStatusBarOrientationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];

    self.DialNumber.delegate = self;
    self.DialNumber.text= @"";

    [self.dialpadButton setTintColor:[UIColor whiteColor]];
    [self.uc.phone setPreviewView:self.localView];

    [ACBClientPhone requestMicrophoneAndCameraPermission:true video:true];
    
    self.audio = YES;
    self.video = YES;
    self.lastNumber = @"";

    [self initialiseArrays];

    for (UIButton* button in self.buttonArray)
    {
        [button.layer setBorderWidth:5.0f];
        [button.layer setCornerRadius:5.0f];
        [button setBackgroundColor:[UIColor blackColor]];
    }
    
    for (UIButton *button in self.buttonArray)
    {
        [button.layer setBorderColor:[CALL_BUTTON_GREY CGColor]];
    }
    
    for (int i=0; i<4; i++)
    {
        [videoViews[i] setHidden:YES];
    }
    
    [self selectCallSlotIndex:0];
    
    _volumeSlider = [[MPVolumeView alloc] initWithFrame:CGRectMake(30, 450, 161, 20)];
    [self.view addSubview:_volumeSlider];
}

- (void) viewDidAppear:(BOOL)animated
{
    self.orientation = [UIApplication sharedApplication].statusBarOrientation;
    [uc.phone setVideoOrientation:self.orientation];
}

- (void) qualitySelectionChanged:(ACBVideoCaptureSetting *)choice
{
    self.selectedQuality = choice;
    uc.phone.preferredCaptureResolution = self.selectedQuality.resolution;
    uc.phone.preferredCaptureFrameRate = self.selectedQuality.frameRate;
    
    CGSize selectedSize = [ACBVideoCaptureSetting sizeForVideoCaptureResolution:self.selectedQuality.resolution];
    NSLog(@"selected quality %@ @ %lufps", NSStringFromCGSize(selectedSize), (unsigned long)self.selectedQuality.frameRate);
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"settingsSegue"])
    {
        self.settingsPopover = [(UIStoryboardPopoverSegue*)segue popoverController];
        SettingsPopoverViewController* vc = [segue destinationViewController];
        vc.qualityChoices = uc.phone.recommendedCaptureSettings;
        vc.currentChoice = self.selectedQuality;
        vc.delegate = self;
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if (self.settingsPopover)
    {
        [self.settingsPopover dismissPopoverAnimated:YES];
        return NO;
    }
    
    return YES;
}

- (void) initialiseArrays
{
    if (!self.buttonArray)
    {
        self.buttonArray = [[NSArray alloc] initWithObjects:self.firstCallButton,
                            self.secondCallButton,
                            self.thirdCallButton,
                            self.fourthCallButton,
                            nil];
    }

    if (!self.buttonLabelArray)
    {
        self.buttonLabelArray = [[NSArray alloc] initWithObjects:self.firstCallButtonLabel,
                            self.secondCallButtonLabel,
                            self.thirdCallButtonLabel,
                            self.fourthCallButtonLabel,
                            nil];
    }

    videoViews[0] = self.videoView1;
    videoViews[1] = self.videoView2;
    videoViews[2] = self.videoView3;
    videoViews[3] = self.videoView4;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) willRotate:(NSNotification *)notification
{
    NSInteger value = [[notification.userInfo objectForKey:UIApplicationStatusBarOrientationUserInfoKey] integerValue];
    self.orientation = (UIInterfaceOrientation)value;
}

- (void) didRotate:(NSNotification *)notification
{
    [self.uc.phone setVideoOrientation:self.orientation];
}

- (IBAction)press1Key:(id)sender
{
    self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"1"];
}

- (IBAction)press2Key:(id)sender
{
    self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"2"];
}

- (IBAction)press3Key:(id)sender
{
    self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"3"];    
}

- (IBAction)press4Key:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"4"];
}

- (IBAction)press5Key:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"5"];
}

- (IBAction)press6Key:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"6"];
}

- (IBAction)press7Key:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"7"];
}

- (IBAction)press8Key:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"8"];
}

- (IBAction)press9Key:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"9"];
}

- (IBAction)pressStarKey:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"*"];
}

- (IBAction)press0Key:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"0"];
}

- (IBAction)pressHashKey:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"#"];
}

- (IBAction)pressPlusKey:(id)sender
{
        self.DialNumber.text = [self.DialNumber.text stringByAppendingString:@"+"];
}

- (IBAction)pressCallKey:(id)sender
{
    self.DialNumber.text = @"";
}

- (IBAction)pressDeleteKey:(id)sender
{
    NSString *number = self.DialNumber.text;
    if (number.length > 0)
    {
        self.DialNumber.text = [number substringToIndex:number.length - 1];
    }
}

- (IBAction)DialKeyPressed:(id)sender
{
    [self Dial];
}

- (void) Dial
{
    
    if (![self.DialNumber.text isEqualToString:@""])
    {
        ACBClientCall* call = [self.uc.phone createCallToAddress:self.DialNumber.text audio:YES video:YES delegate:self];
        
        if (call)
        {
            [self setCall:call atIndex:self.selectedCallIndex];
            [self switchToInCallUI];
        
            [self setAudioEnabledState:YES];
            [self setVideoEnabledState:YES];
        
            self.lastNumber = self.DialNumber.text;
            self.DialNumber.text = @"";
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"ERROR (for call)" message:@"A call must be created with media." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        
    }
    else
    {
        self.DialNumber.text = self.lastNumber;
    }
}

- (IBAction)pressEndCall:(UIButton *)sender
{
    [calls[self.selectedCallIndex] end];
}

- (IBAction)pressMute:(id)sender
{
    [self setAudioEnabledState:!self.audio];
}

- (IBAction)pressVideo:(id)sender
{
    [self setVideoEnabledState:!self.video];
}

- (void)setAudioEnabledState:(BOOL)enabledState
{
    int selectedCallIndex = self.selectedCallIndex;
    [calls[selectedCallIndex] enableLocalAudio:enabledState];
    audioEnabled[selectedCallIndex] = enabledState;

    [self setAudioMuteIconForAudioState:enabledState];

    _audio = enabledState;
}

- (void)setAudioMuteIconForAudioState:(bool)enabledState
{
    UIImage *image = [UIImage imageNamed: (enabledState ? IMAGE_UNMUTE : IMAGE_MUTE)];
    [_muteButton setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)setVideoEnabledState:(BOOL)videoState
{
    int selectedCallIndex = self.selectedCallIndex;
    [calls[selectedCallIndex] enableLocalVideo:videoState];
    videoEnabled[selectedCallIndex] = videoState;

    [self setVideoMuteIcon:videoState];

    _video = videoState;
}

- (void)setVideoMuteIcon:(bool)videoState
{
    UIImage *image = [UIImage imageNamed: (videoState ? IMAGE_VIDEO_ENABLE : IMAGE_VIDEO_DISABLE)];
    [_videoButton setBackgroundImage:image forState:UIControlStateNormal];
}

- (void) call:(ACBClientCall*)call didReceiveCallFailureWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"ERROR (for call)" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void) phone:(ACBClientPhone*)phone didReceiveCall:(ACBClientCall*)call
{
    int newSlotNumber = [self firstEmptySlot];
    if (newSlotNumber < 0)
	{
        // 4 calls are currently active: decline
		[call end];
		return;
	}

    self.lastIncomingCall = call;
    [self playRingtone];

    // We need to temporarily assign ourselves as the call's delegate so that we get notified if it ends before we answer it.
    [call setDelegate:self];

    [self initialiseArrays];

	NSString *callType;
    if (call.hasRemoteAudio && call.hasRemoteVideo)
    {
		callType = @"video";
    }
    else if (call.hasRemoteAudio)
    {
		callType = @"";
    }
    else if (call.hasRemoteVideo)
    {
		callType = @"video-only";
    }
	NSString *title =   [NSString stringWithFormat:@"Incoming %@ call", callType];
    NSString *message;

    if (call.remoteDisplayName == nil)
    {
        message = [NSString stringWithFormat:@"Incoming %@ call from %@.", callType, call.remoteAddress];
    }
    else
    {
        message = [NSString stringWithFormat:@"Incoming %@ call from %@.", callType, call.remoteDisplayName];
    }

	self.incomingCallAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ignore" otherButtonTitles:@"Answer", nil];

    // Note that UIAlertView will not appear if your application is running in the background - use UILocalNotification instead.

    [self.incomingCallAlert show];

    // If we're in the background then pop up a local notification.
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive)
    {
        self.incomingCallNotification = [[UILocalNotification alloc] init];
        self.incomingCallNotification.fireDate = [[NSDate date] dateByAddingTimeInterval: 0];
        self.incomingCallNotification.repeatInterval = 0;
        self.incomingCallNotification.alertAction = @"Answer";
        self.incomingCallNotification.alertBody = [NSString stringWithFormat: @"Incoming call from %@.", message];
        self.incomingCallNotification.soundName = @"ringring.wav";
        [[UIApplication sharedApplication] scheduleLocalNotification:self.incomingCallNotification];
    }
}

- (void) call:(ACBClientCall *)call didReceiveDialFailureWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"ERROR (for call)" message:@"The call could not be connected." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self switchToNotInCallUI];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	self.incomingCallAlert = nil;
    [self stopRingingIfNoOtherCallIsRinging:self.lastIncomingCall];

	if (buttonIndex == 1)
	{
        [(UITabBarController*)[self parentViewController] setSelectedIndex:0];

        // Slot availability has already been checked before displaying the alert
        ACBClientCall *newCall = self.lastIncomingCall;
        int newSlotNumber = [self firstEmptySlot];
        [self setCall:newCall atIndex:newSlotNumber];
        [self switchToCall:newSlotNumber];
        [self switchToInCallUI];
        [newCall answerWithAudio:YES video:YES];
	}
	else
	{
		[self.lastIncomingCall end];
	}

    self.lastIncomingCall = nil;
    [self setAudioEnabledState:YES];
    [self setVideoEnabledState:YES];
}

- (void) didRotateFromInterfaceOrientation: (UIInterfaceOrientation) fromInterfaceOrientation {
    if (self.dialpadPopover.isPopoverVisible) {
        [self.dialpadPopover dismissPopoverAnimated: YES];
        self.dialpadPopover = nil;
        [self displayDialpad:nil];
    }
}

- (void) callDidReceiveMediaChangeRequest:(ACBClientCall *)call
{
}

- (void)displayDialpad:(UIButton *)sender
{
    KeypadPopupViewController* keypadPopup = [[KeypadPopupViewController alloc] init];
    [keypadPopup setDelegate:self];
    self.dialpadPopover = [[UIPopoverController alloc] initWithContentViewController:keypadPopup];

    CGRect rect = CGRectMake(self.dialpadButton.frame.origin.x,
                             self.dialpadButton.frame.origin.y - 10,
                             self.dialpadButton.frame.size.width,
                             self.dialpadButton.frame.size.height);
    [self.dialpadPopover setPopoverContentSize:CGSizeMake(318, 268)];
    [self.dialpadPopover presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void)DTMFButtonPressed:(UIButton *)button
{
    if (button.tag >= 0 && button.tag <= 9)
    {
        NSLog(@"%li pressed", (long)button.tag);
        [calls[self.selectedCallIndex] playDTMFCode:[NSString stringWithFormat:@"%li",(long)button.tag] localPlayback:YES];
    }
    else if (button.tag == 11)
    {
        NSLog(@"* pressed");
        [calls[self.selectedCallIndex] playDTMFCode:@"*" localPlayback:YES];
    }
    else
    {
        NSLog(@"# pressed");
        [calls[self.selectedCallIndex] playDTMFCode:@"#" localPlayback:YES];
    }
}


- (void) call:(ACBClientCall*)call didReceiveCallRecordingPermissionFailure:(NSString *)message
{
    [[[UIAlertView alloc] initWithTitle:@"ERROR (for call)" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

/** A callback notification indicating that the local media stream was added. */
- (void) callDidAddLocalMediaStream:(ACBClientCall *)call
{
}

/** A callback notification indicating that the remote media stream was added. */
- (void) callDidAddRemoteMediaStream:(ACBClientCall *)call
{
}

- (void) call:(ACBClientCall*)call didChangeStatus:(ACBClientCallStatus)status
{
    switch (status)
    {
        case ACBClientCallStatusRinging:
            [self playRingtone];
            break;
        case ACBClientCallStatusAlerting:
            break;
        case ACBClientCallStatusMediaPending:
            break;
        case ACBClientCallStatusInCall:
            [self stopRingingIfNoOtherCallIsRinging:nil];
            break;
        case ACBClientCallStatusEnded:
            [self updateUIForEndedCall:call];
            break;
        case ACBClientCallStatusSetup:
            break;
        case ACBClientCallStatusBusy:
        case ACBClientCallStatusError:
        case ACBClientCallStatusNotFound:
        case ACBClientCallStatusTimedOut:
            [self updateUIForEndedCall:call];
            break;
    }
}

- (void) stopRingingIfNoOtherCallIsRinging:(ACBClientCall *)call
{
    // Multiple incoming calls can still mess up
    if (self.lastIncomingCall && (self.lastIncomingCall != call))
    {
        return;
    }
    
    for (int i=0; i<4; i++)
    {
        ACBClientCallStatus status = calls[i].status;
        if ((status == ACBClientCallStatusRinging) || (status == ACBClientCallStatusAlerting))
        {
            return;
        }
    }
    
    [self stopRingtone];
}

- (void) updateUIForEndedCall:(ACBClientCall *)call
{
    if (call == self.lastIncomingCall)
    {
        self.lastIncomingCall = nil;
        [self.incomingCallAlert dismissWithClickedButtonIndex:0 animated:YES];
        if (self.incomingCallNotification)
        {
            [[UIApplication sharedApplication] cancelLocalNotification:self.incomingCallNotification];
            self.incomingCallNotification = nil;
        }
    }
    [self stopRingingIfNoOtherCallIsRinging:nil];
    
    int callIndex = [self indexOfCall:call];

    if (callIndex >= 0)
    {
        if (callIndex == self.selectedCallIndex)
        {
            [self switchToNotInCallUI];
        }
    
        [[self.buttonLabelArray objectAtIndex:callIndex] setText:@"No call"];
        calls[callIndex] = nil;
        if (callIndex == self.selectedCallIndex)
        {
            [self.dialpadPopover dismissPopoverAnimated:NO];
        }
        else
        {
            // Rather than "unselect" is just an "update unselected"
            [self unselectCallSlotIndex:callIndex];
        }
    }
}

- (void) call:call didReportInboundQualityChange:(NSUInteger)inboundQuality
{
    self.callQualityView.quality = inboundQuality;
}

- (int) indexOfCall:(ACBClientCall *)call
{
    for (int i=0; i<4; i++)
    {
        if (calls[i] == call)
        {
            return i;
        }
    }

    return -1;
}

- (int) firstEmptySlot
{
    return [self indexOfCall:nil];
}

- (void) switchToInCallUI
{
    [self.dialPad setHidden:YES];
    [self.dialpadButton setHidden:NO];
    [self.settingsButton setHidden:YES];
    [self.callControls setHidden:NO];
    [self.localView setHidden:NO];
    [self.callQualityView setHidden:NO];
}

- (void) switchToNotInCallUI
{
    [self.dialPad setHidden:NO];
    [self.dialpadButton setHidden:YES];
    [self.settingsButton setHidden:NO];
    [self.callControls setHidden:YES];
    [self.localView setHidden:YES];
    [self.callQualityView setHidden:YES];
}

- (IBAction)firstCallButtonPressed:(UIButton *)sender
{
    [self switchToCall:0];
}

- (IBAction)secondCallButtonPressed:(UIButton *)sender
{
    [self switchToCall:1];
}

- (IBAction)thirdCallButtonPressed:(UIButton *)sender
{
    [self switchToCall:2];
}

- (IBAction)fourthCallButtonPressed:(UIButton *)sender
{
    [self switchToCall:3];
}

- (IBAction)settingsButtonPressed:(UIButton *)sender
{
}

- (void)unselectCallSlotIndex:(int)callSlotIndex
{
    UIButton *newButton = [self.buttonArray objectAtIndex:callSlotIndex];
    ACBClientCall *call = calls[callSlotIndex];
    UIColor *color = (call == nil ? CALL_BUTTON_GREY : CALL_BUTTON_RED);
    [newButton.layer setBorderColor:[color CGColor]];
    UIView *newView = videoViews[callSlotIndex];
    [newView setHidden:YES];
}

- (void)selectCallSlotIndex:(int)callSlotNumber
{
    UIButton *newButton = [self.buttonArray objectAtIndex:callSlotNumber];
    [newButton.layer setBorderColor:[CALL_BUTTON_GREEN CGColor]];
    UIView *newView = videoViews[callSlotNumber];
    [newView setHidden:NO];
}

- (void)switchToCall:(int)newCallIndex
{
    int oldCallIndex = self.selectedCallIndex;
    if (newCallIndex == oldCallIndex)
    {
        return;
    }

    [calls[oldCallIndex] hold];
    [calls[newCallIndex] resume];
    
    [[self.buttonArray objectAtIndex:oldCallIndex] setBackgroundImage:[self takeVideoSnapshotOfView:videoViews[oldCallIndex]] forState:UIControlStateNormal];
    
    self.selectedCallIndex = newCallIndex;

    [self unselectCallSlotIndex:oldCallIndex];
    [self selectCallSlotIndex:newCallIndex];

    if (calls[newCallIndex])
    {
        [self switchToInCallUI];

        [self setAudioMuteIconForAudioState:audioEnabled[newCallIndex]];
        self.audio = audioEnabled[newCallIndex];

        [self setVideoMuteIcon:videoEnabled[newCallIndex]];
        self.video = videoEnabled[newCallIndex];
    }
    else
    {
        [self switchToNotInCallUI];
    }
}

- (void) setCall:(ACBClientCall *)call atIndex:(int)index
{
    calls[index] = call;
    UILabel *label = [self.buttonLabelArray objectAtIndex:index];
    label.text = [call remoteAddress];
    [call setVideoView:videoViews[index]];
}

- (void)playRingtone
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDuckOthers error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    NSString* filename = @"ringring";
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"wav"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];

    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:NULL];
    self.audioPlayer.volume = 1.0;

    // Minus number makes ringtone play indefinitely
    self.audioPlayer.numberOfLoops = -1;
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
}

- (void)stopRingtone
{
    if (self.audioPlayer)
    {
        [self.audioPlayer stop];
    }
}

// This method is used to take a screenshot of a view.
// The resulting image is then used for the preview image on the call buttons.
- (UIImage *)takeVideoSnapshotOfView:(UIView*)glView
{
    // Draw OpenGL data to a UIImage
    CGFloat scale = UIScreen.mainScreen.scale;
    CGFloat xOffset = 40.0f;
    CGFloat yOffset = -16.0f;
    CGSize size = CGSizeMake((glView.frame.size.width) * scale,
                             glView.frame.size.height * scale);

    //Create buffer for pixels
    GLuint bufferLength = size.width * size.height * 4;
    GLubyte* buffer = (GLubyte*)malloc(bufferLength);

    //Read Pixels from OpenGL
    glReadPixels(0.0f, 0.0f, size.width, size.height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

    //Make data provider with data.
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, bufferLength, NULL);

    //Configure image
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * size.width;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGImageAlphaLast;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    CGImageRef iref = CGImageCreate(size.width, size.height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);

    uint32_t* pixels = (uint32_t*)malloc(bufferLength);
    CGContextRef context = CGBitmapContextCreate(pixels, size.width, size.height, 8, size.width * 4, CGImageGetColorSpace(iref), kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);

    CGContextTranslateCTM(context, 0.0f, size.height);
    CGContextScaleCTM(context, 1.0f, -1.0f);

    // These numbers are a little magical.
    CGContextDrawImage(context, CGRectMake(xOffset, yOffset, ((size.width - (6.0f * scale)) / scale) - (xOffset / 2), (size.height / scale) - (yOffset / 2)), iref);
    UIImage *outputImage = [UIImage imageWithCGImage:CGBitmapContextCreateImage(context)];

    //Dealloc
    CGDataProviderRelease(provider);
    CGImageRelease(iref);
    CGContextRelease(context);
    free(buffer);
    free(pixels);

    return outputImage;
}

- (void) phone:(ACBClientPhone*)phone didChangeCaptureSetting:(ACBVideoCaptureSetting*)settings forCamera:(AVCaptureDevicePosition)camera
{
    NSLog(@"didChangeCaptureSetting - resolution=%d frame rate= %lu camera=%ld", settings.resolution, (unsigned long)settings.frameRate, camera);
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.DialNumber)
    {
        [textField resignFirstResponder];
        
        [self Dial];
    }
    
    return TRUE;
}
    
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:TRUE];
    [super touchesBegan:touches withEvent:event];
}

@end
