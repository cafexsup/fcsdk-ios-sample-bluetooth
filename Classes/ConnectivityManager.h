//
//  ConnectivityManager.h
//
//  Created by DevTest2 on 04/03/2013.
//

#import <Foundation/Foundation.h>
#import <ACBClientSDK/ACBUC.h>
#import "ClientHTTPSConnectionHandler.h"
#import "ReachabilityManager.h"

@interface ConnectivityManager : NSObject<HTTPSConnectionHandlerDelegate, ACBUCDelegate, ReachabilityManagerListener>

- (void)loginWithHider:(BOOL)hiderOn;
- (void)logout;

@end
