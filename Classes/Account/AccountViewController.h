//
//  AccountViewController.h
//  ImPresenceSample
//
//  Created by DevTest2 on 08/02/2013.
//

#import <UIKit/UIKit.h>
#import "UCClientTabbedViewController.h"
#import "ClientHTTPSConnectionHandler.h"

@interface AccountViewController : UIViewController<UCSessionHandler>

- (IBAction)logoutButtonAction:(id)sender;

@end
