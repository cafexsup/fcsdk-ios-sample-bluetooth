//
//  LoginViewController.h
//  ImPresenceSample
//
//  Created by Tom Greenwood on 16/01/2013.
//

#import <Foundation/Foundation.h>
#import <ACBClientSDK/ACBUC.h>
#import "ClientHTTPSConnectionHandler.h"

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *versionField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *serverField;
@property (weak, nonatomic) IBOutlet UITextField *portField;
@property (weak, nonatomic) IBOutlet UISwitch *secureSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *untrustedCertificatesSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *useCookiesSwitch;
@property (strong) ACBUC *uc;
@property (strong) NSString *configuration;
- (IBAction)loginPress:(id)sender;
- (IBAction)cancelLoginPress:(id)sender;

@end