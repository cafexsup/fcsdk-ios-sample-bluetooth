//
//  LoginViewController.m
//  ImPresenceSample
//
//  Created by Tom Greenwood on 16/01/2013.
//

#import "ConnectivityManager.h"
#import "ImSampleAppDelegate.h"
#import "LoginViewController.h"
#import <ACBClientSDK/ACBUC.h>
#import <ACBClientSDK/ACBClientVersion.h>
#import "UCClientTabbedViewController.h"

@implementation LoginViewController

- (void)viewDidLoad
{
    [[[self view] window] superview];

	ImSampleAppDelegate *app = (ImSampleAppDelegate *)[UIApplication sharedApplication].delegate;
	app.loginViewController = self;

    self.versionField.text = SDK_VERSION_NUMBER;
    
	self.userNameField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
	self.passwordField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
	self.serverField.text   = [[NSUserDefaults standardUserDefaults] objectForKey:@"server"];
    
    NSNumber *port = [[NSUserDefaults standardUserDefaults] objectForKey:@"port"];
    NSString *portString = [[NSString alloc] initWithFormat:@"%@", port];
    self.portField.text = portString;

    [self.userNameField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.passwordField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.serverField   setAutocorrectionType:UITextAutocorrectionTypeNo];

    [self.userNameField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.passwordField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.serverField   setAutocapitalizationType:UITextAutocapitalizationTypeNone];

    NSNumber *secureNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"secureSwitch"];
    BOOL secure = [secureNumber boolValue];
    [self.secureSwitch setOn:secure];

    NSNumber *acceptUntrustedCertificatesNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"acceptUntrustedCertificates"];
    BOOL acceptUntrustedCertificates = [acceptUntrustedCertificatesNumber boolValue];
    [self.untrustedCertificatesSwitch setOn:acceptUntrustedCertificates];
    
    NSNumber *useCookiesNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"useCookies"];
    BOOL useCookies = [useCookiesNumber boolValue];
    [self.useCookiesSwitch setOn:useCookies];
    
}

- (IBAction)loginPress:(id)sender
{
	[[NSUserDefaults standardUserDefaults] setObject:self.userNameField.text forKey:@"username"];
	[[NSUserDefaults standardUserDefaults] setObject:self.passwordField.text forKey:@"password"];
	[[NSUserDefaults standardUserDefaults] setObject:self.serverField.text   forKey:@"server"];
    [[NSUserDefaults standardUserDefaults] setObject:self.portField.text     forKey:@"port"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:self.secureSwitch.on] forKey:@"secureSwitch"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:self.untrustedCertificatesSwitch.on] forKey:@"acceptUntrustedCertificates"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:self.useCookiesSwitch.on] forKey:@"useCookies"];
	ImSampleAppDelegate *appDelegate = (ImSampleAppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.userWantsToBeLoggedIn = YES;
	[appDelegate.connectivityManager loginWithHider:YES];
}

- (IBAction)cancelLoginPress:(id)sender
{
    exit(0);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"loginSegue"])
    {
        UCClientTabbedViewController *imView = (UCClientTabbedViewController *)segue.destinationViewController;
        imView.configuration = self.configuration;
        imView.server = [[NSUserDefaults standardUserDefaults] objectForKey:@"server"];

		imView.uc = _uc;

        [[[self view] window] setRootViewController:imView];
    }
}

@end