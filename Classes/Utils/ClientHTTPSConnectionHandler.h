//
//  HttpConnection.h
//  HttpsConnectionDemo
//
//  Created by Marco Dalco on 30/09/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HTTPSConnectionHandlerDelegate
    -(void) dataLoaded:(NSData *)nsData;
    -(void) errorHappened:(NSError *)error;
@end

@interface ClientHTTPSConnectionHandler : NSObject
    -(id)initWithUrlString:(NSString *)urlString acceptUntrustedCertificates:(BOOL)acceptUntrustedCertificates method:(NSString*)method
                andHeaders:(NSDictionary *)headers andPayload:(NSData *)payload
                andNotify:(NSObject<HTTPSConnectionHandlerDelegate> *)theReceiver;

@end