//
//  ContactsTableDataSource.m
//  ImPresenceSample
//
//  Created by Tom Greenwood on 10/02/2013.
//

#import "ContactsTableDataSource.h"

@implementation ContactsTableDataSource
{
    ACBClientPresence *_presence;
    NSMutableDictionary *_pendingMessages;
}

-(id)initWithPresence:(ACBClientPresence *)presence
{
    if (self = [self init])
    {
        _presence = presence;
        _pendingMessages = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void)setHasIncomingPendingMessages:(BOOL)hasMessage forContact:(NSString *)contact
{
    [_pendingMessages setObject:[NSNumber numberWithBool:hasMessage] forKey:contact];
}

#pragma mark -
#pragma mark UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = [_presence.contacts count];
    NSLog(@"Currently have %lu contacts", (unsigned long)count);
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    ACBClientContact *contact = [_presence.contacts objectAtIndex:row];

    NSString *reuseId;
    switch (contact.status)
    {
        case ACBClientPresenceStatusAvailable:
            reuseId = @"Available";
            break;
        case ACBClientPresenceStatusAway:
            reuseId = @"Away";
            break;
        case ACBClientPresenceStatusBusy:
            reuseId = @"Busy";
            break;
        case ACBClientPresenceStatusOffline:
        default:
            reuseId = @"Offline";
            break;
    }

    BOOL hasPendingMessages = [[_pendingMessages objectForKey:contact.address] boolValue];
    if (hasPendingMessages)
    {
        reuseId = [reuseId stringByAppendingString:@"Pending"];
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
    }

    cell.textLabel.text = contact.address;
    cell.detailTextLabel.text = contact.customMessage;
    cell.backgroundColor = [UIColor darkGrayColor];

    NSLog(@"Row %ld reuseId %@ status %d %@ contact %@", (long)row, reuseId, contact.status, contact.customMessage, contact.address);

    return cell;
}

@end