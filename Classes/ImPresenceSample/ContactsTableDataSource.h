//
//  ContactsTableDataSource.h
//  ImPresenceSample
//
//  Created by Tom Greenwood on 10/02/2013.
//

#import <Foundation/Foundation.h>
#import <ACBClientSDK/ACBClientPresence.h>

@interface ContactsTableDataSource : NSObject <UITableViewDataSource>

-(id)initWithPresence:(ACBClientPresence *)presence;
-(void)setHasIncomingPendingMessages:(BOOL)hasMessage forContact:(NSString*)contact;

@end
