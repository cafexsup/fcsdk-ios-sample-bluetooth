//
//  ImSampleViewController.h
//  ImPresenceSample
//
//  Created by Tom Greenwood on 16/01/2013.
//

#import <UIKit/UIKit.h>
#import "ChangeStateViewController.h"
#import "UCClientTabbedViewController.h"
#import <ACBClientSDK/ACBUC.h>
#import <ACBClientSDK/ACBClientPresence.h>

@interface ImSampleViewController : UIViewController <UIGestureRecognizerDelegate, ChangeStateDelegate, UCConsumer, ACBClientPresenceDelegate, ACBClientConversationDelegate, ACBClientMessageDelegate, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *contactsTable;
@property NSString *server;
@property NSString *configuration;
@property (weak, nonatomic) IBOutlet UIImageView *presenceStateImage;
@property (weak, nonatomic) IBOutlet UILabel *presenceStateLabel;
@property (weak, nonatomic) IBOutlet UITableView *conversationTable;
@property (weak, nonatomic) IBOutlet UITextView *conversationInputView;
@property (weak, nonatomic) IBOutlet UIButton *conversationInputButton;
@property (weak, nonatomic) IBOutlet UIButton *conversationEndButton;

- (IBAction) sendMessage:(id)sender;
- (IBAction) endConversation:(id)sender;

@end