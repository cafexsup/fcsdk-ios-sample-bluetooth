//
//  ChangeStateViewController.h
//  ImPresenceSample
//
//  Created by Tom Greenwood on 21/01/2013.
//

#import <Foundation/Foundation.h>
#import <ACBClientSDK/ACBClientPresence.h>

@protocol ChangeStateDelegate <NSObject>
-(ACBClientPresenceStatus) getState;
-(NSString *)getCustomMessage;
- (void)changeState:(ACBClientPresenceStatus)state withMessage:(NSString *)message;
@end

@interface ChangeStateViewController: UIViewController <UITableViewDelegate, UITableViewDataSource>
    @property (assign)id<ChangeStateDelegate> delegate;
    @property (nonatomic) IBOutlet UITextField *customStateText;
    @property (retain)UIImage *availableImage;
    @property (retain)UIImage *awayImage;
    @property (retain)UIImage *busyImage;
@end
