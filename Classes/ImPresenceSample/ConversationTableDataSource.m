//
//  ConversationTableDataSource.m
//
//  Created by Ceri Hughes on 14/03/2013.
//

#import "ConversationTableDataSource.h"

@interface ConversationTableDataSource()

@end

@implementation ConversationTableDataSource
{
    ACBClientConversation *_conversation;
}

- (id) initWithConversation:(ACBClientConversation *)conversation
{
    if (self = [self init])
    {
        _conversation = conversation;
    }
    return self;
}

- (ACBClientMessage*) sendMessage:(NSString *)text delegate:(id<ACBClientMessageDelegate>)delegate
{
    return [_conversation sendMessage:text delegate:delegate];
}

- (void) end
{
    [_conversation end];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_conversation.messages count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    ACBClientMessage *message = [_conversation.messages objectAtIndex:row];

    NSString *reuseId;
    switch (message.status)
    {
        case ACBClientMessageStatusSending:
            reuseId = @"Sending";
            break;
        case ACBClientMessageStatusSent:
        default:
            reuseId = @"Sent";
            break;
        case ACBClientMessageStatusFailedToSend:
            reuseId = @"Failed";
            break;
        case ACBClientMessageStatusReceived:
            reuseId = @"Received";
            break;
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
    }

    cell.textLabel.text = message.text;
    cell.backgroundColor = [UIColor darkGrayColor];

    return cell;
}

@end