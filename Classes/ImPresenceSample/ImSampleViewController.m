//
//  ImSampleViewController.m
//  ImPresenceSample
//
//  Created by Tom Greenwood on 16/01/2013.
//

#import "ImSampleViewController.h"
#import "ClientHTTPSConnectionHandler.h"
#import "ContactsTableDataSource.h"
#import "ConversationTableDataSource.h"

@interface ImSampleViewController ()

@end

@implementation ImSampleViewController
{
    ContactsTableDataSource *contactsTableDataSource;
    NSMutableDictionary *conversationDataSources;
    UIImage *availableImage;
    UIImage *busyImage;
    UIImage *awayImage;
    UIImage *offlineImage;

    ACBClientPresenceStatus currentState;
    NSString *currentMessage;
    UIPopoverController *statusPopover;
}

@synthesize uc;

-(void)createImages
{
    availableImage = [UIImage imageNamed:@"available.png"];
    busyImage      = [UIImage imageNamed:@"busy.png"];
    awayImage      = [UIImage imageNamed:@"away.png"];
    offlineImage   = [UIImage imageNamed:@"offline.png"];
}

- (void)setUc:(ACBUC *)theUC
{
	uc = theUC;
	uc.presence.delegate = self;

    // Become the delegate of all current conversations...
    for (ACBClientConversation* conversation in uc.presence.currentConversations)
    {
        conversation.delegate = self;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self createImages];

    contactsTableDataSource = [[ContactsTableDataSource alloc] initWithPresence:uc.presence];
    _contactsTable.dataSource = contactsTableDataSource;
    _contactsTable.delegate = self;

    conversationDataSources = [NSMutableDictionary dictionary];
    [self.uc.presence setStatus:ACBClientPresenceStatusAvailable customMessage:nil];
}

-(void)changeState:(ACBClientPresenceStatus)state withMessage:(NSString *)message
{
    [self stateChanged:state withMessage:message];
    if (message != nil)
    {
        self.presenceStateLabel.text = message;
        [self.uc.presence setStatus:state customMessage:message];
    }
    else
    {
        [self.uc.presence setStatus:state];
    }
}

-(void)stateChanged:(ACBClientPresenceStatus)state withMessage:(NSString *)message
{
    currentState = state;
    currentMessage = message;
    switch (state)
    {
        case ACBClientPresenceStatusAvailable:
            self.presenceStateLabel.text = @"Available";
            self.presenceStateImage.image = availableImage;
            break;
        case ACBClientPresenceStatusBusy:
            self.presenceStateLabel.text = @"Busy";
            self.presenceStateImage.image = busyImage;
            break;
        case ACBClientPresenceStatusAway:
            self.presenceStateLabel.text = @"Away";
            self.presenceStateImage.image = awayImage;
            break;
        case ACBClientPresenceStatusOffline:
            self.presenceStateLabel.text = @"Offline";
            self.presenceStateImage.image = offlineImage;
            break;
        default:
            self.presenceStateLabel.text = @"Unknown";
            self.presenceStateImage.hidden = YES;
            break;
    }
}

- (IBAction) sendMessage:(id)sender
{
    NSString *address = [self contactAddressAtSelectedIndex];
    if (address != nil)
    {
        ConversationTableDataSource *dataSource = [conversationDataSources objectForKey:address];
        [dataSource sendMessage:self.conversationInputView.text delegate:self];
        [self updateConversationTable];
        self.conversationInputView.text = @"";
    }
}

- (IBAction) endConversation:(id)sender
{
    NSString *address = [self contactAddressAtSelectedIndex];
    if (address != nil)
    {
        self.conversationTable.dataSource = nil;
        ConversationTableDataSource *dataSource = [conversationDataSources objectForKey:address];
        [dataSource end];
        [conversationDataSources removeObjectForKey:address];
        [self.contactsTable selectRowAtIndexPath:nil animated:NO scrollPosition:UITableViewScrollPositionNone];
        [self refreshContactsTable];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"changePresence"])
    {
        ChangeStateViewController *vc = segue.destinationViewController;
        statusPopover = [(UIStoryboardPopoverSegue *)segue popoverController];
        vc.delegate = self;
        vc.availableImage = availableImage;
        vc.awayImage = awayImage;
        vc.busyImage = busyImage;
        NSLog(@"Segue for change presence");
    }
    else
    {
        NSLog(@"Unknown segue");
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (statusPopover)
    {
        [statusPopover dismissPopoverAnimated:YES];
    }
}

-(ACBClientPresenceStatus)getState
{
    return currentState;
}

- (NSString *)getCustomMessage
{
    return currentMessage;
}

- (NSUInteger) indexOfContactAddress:(NSString*)address
{
    NSUInteger index = 0;
    for (ACBClientContact *contact in uc.presence.contacts)
    {
        if ([contact.address isEqualToString:address])
        {
            return index;
        }
        index++;
    }
    return NSNotFound;
}

- (NSString*) contactAddressAtIndex:(NSUInteger)contactIndex
{
    ACBClientContact *contact = [uc.presence.contacts objectAtIndex:contactIndex];
    return contact.address;
}

- (NSString*) contactAddressAtSelectedIndex
{
    NSIndexPath *selectedPath = [self.contactsTable indexPathForSelectedRow];
    if (selectedPath != nil)
    {
        NSInteger selectedRow = selectedPath.row;
        return [self contactAddressAtIndex:selectedRow];
    }
    return nil;
}

- (void) contactAddress:(NSString*)address hasNotification:(BOOL)hasNotification
{
    [contactsTableDataSource setHasIncomingPendingMessages:hasNotification forContact:address];
    [self refreshContactsTable];
}

- (BOOL) viewingConversationWithContactAddress:(NSString*)contactAddress
{
    return [[self contactAddressAtSelectedIndex] isEqualToString:contactAddress];
}

- (void) updateConversationTable
{
    [self.conversationTable reloadData];
    NSUInteger messageCount = [self.conversationTable numberOfRowsInSection:0];
    if (messageCount > 0)
    {
        NSIndexPath *path = [NSIndexPath indexPathForRow:messageCount - 1 inSection:0];
        [self.conversationTable scrollToRowAtIndexPath:path
                                      atScrollPosition:UITableViewScrollPositionNone animated:YES];
    }
}

#pragma mark -
#pragma mark ACBClientPresenceDelegate

- (void) presence:(ACBClientPresence*)presence didReceiveUserStatusChange:(ACBClientPresenceStatus)status customMessage:(NSString *)customMessage
{
    [self stateChanged:status withMessage:customMessage];
}

- (void) refreshContactsTable
{
    NSString *selectedAddress = [self contactAddressAtSelectedIndex];
    [_contactsTable reloadData];
    BOOL hideConversation = (selectedAddress == nil);
    self.conversationTable.hidden = hideConversation;
    self.conversationInputView.hidden = hideConversation;
    self.conversationInputButton.hidden = hideConversation;
    self.conversationEndButton.hidden = hideConversation;
    if (!hideConversation)
    {
        NSUInteger newIndex = [self indexOfContactAddress:selectedAddress];
        NSIndexPath *path = [NSIndexPath indexPathForRow:newIndex inSection:0];
        [_contactsTable selectRowAtIndexPath:path animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void) presence:(ACBClientPresence*)presence contact:(NSString *)contactId didReceiveStatusChange:(ACBClientPresenceStatus)status customMessage:(NSString *)customMessage
{
    [self refreshContactsTable];
}

- (void) presence:(ACBClientPresence*)presence didStartConversation:(ACBClientConversation*)conversation
{
    [self conversationDidStart:conversation];
}

#pragma mark -
#pragma mark ACBClientConversationDelegate

- (void) conversation:(ACBClientConversation*)conversation didReceiveMessage:(ACBClientMessage*)message
{
    NSString *address = conversation.contactAddress;
    if ([self viewingConversationWithContactAddress:address])
    {
        // We're already looking at this conversation... reload the table...
        [self updateConversationTable];
    }
    else
    {
        // Notify that a message has come in on another conversation...
        [self contactAddress:address hasNotification:YES];
        [self refreshContactsTable];
    }

    // If we're in the background then pop up a local notification.
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive)
    {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [[NSDate date] dateByAddingTimeInterval: 0];
        localNotification.repeatInterval = 0;
        localNotification.alertAction = @"View";
        localNotification.alertBody = [NSString stringWithFormat: @"%@ [%@]", message.text, conversation.contactAddress];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}

- (void) conversationDidStart:(ACBClientConversation*)conversation
{
    conversation.delegate = self;
    ConversationTableDataSource* dataSource = [[ConversationTableDataSource alloc] initWithConversation:conversation];
    NSString *address = conversation.contactAddress;
    if ([self viewingConversationWithContactAddress:address])
    {
        // We're looking at this conversation... reload the table...
        self.conversationTable.dataSource = dataSource;
    }
    [conversationDataSources setObject:dataSource forKey:conversation.contactAddress];
}

- (void) conversationDidFailToStart:(ACBClientConversation*)conversation
{
    NSString *address = conversation.contactAddress;
    if ([self viewingConversationWithContactAddress:address])
    {
        // We're looking at this conversation... empty the table...
        self.conversationTable.dataSource = nil;
    }
    [conversationDataSources removeObjectForKey:address];
}

- (void) conversationDidEnd:(ACBClientConversation*)conversation
{
}

#pragma mark -
#pragma mark ACBClientMessageDelegate

- (void) message:(ACBClientMessage*)message didReceiveDeliveryConfirmationInConversation:(ACBClientConversation *)conversation
{
    NSString *address = conversation.contactAddress;
    if ([self viewingConversationWithContactAddress:address])
    {
        [self updateConversationTable];
    }
}

- (void) message:(ACBClientMessage*)message didReceiveDeliveryFailureInConversation:(ACBClientConversation *)conversation
{
    NSString *address = conversation.contactAddress;
    if ([self viewingConversationWithContactAddress:address])
    {
        [self updateConversationTable];
    }
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    NSString *address = [self contactAddressAtIndex:row];
    [self contactAddress:address hasNotification:NO];
    ConversationTableDataSource *dataSource = [conversationDataSources objectForKey:address];
    if (dataSource == nil)
    {
        ACBClientConversation *conversation = [uc.presence startConversationWithContact:address delegate:self];
        dataSource = [[ConversationTableDataSource alloc] initWithConversation:conversation];
        [conversationDataSources setObject:dataSource forKey:conversation.contactAddress];
    }
    self.conversationTable.dataSource = dataSource;
    [self updateConversationTable];
}

@end