//
//  ChangeStateViewController.m
//  ImPresenceSample
//
//  Created by Tom Greenwood on 21/01/2013.
//

#import "ChangeStateViewController.h"

@implementation ChangeStateViewController
{
    NSIndexPath *oldPath;
}

-(void)viewDidLoad
{
    int row = 0;
    switch ([self.delegate getState])
    {
        case ACBClientPresenceStatusAvailable:
            row = 0;
            break;
        case ACBClientPresenceStatusBusy:
            row = 1;
            break;
        default:
            row = 2;
            break;
    }
}

- (IBAction)finishedChangingCustomMessage:(id)sender
{
    NSString *customMessage = self.customStateText.text;
    if (customMessage.length == 0)
    {
        customMessage = Nil;
    }
    [self.delegate changeState:[self.delegate getState] withMessage:customMessage];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"State";
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Unknown";
    NSInteger row = indexPath.row;
    if (row == 0)
    {
        cellIdentifier = @"Available";
    }
    else if (row == 1)
    {
        cellIdentifier = @"Busy";
    }
    else if (row == 2)
    {
        cellIdentifier = @"Away";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    int currentRow = 0;
    switch ([self.delegate getState])
    {
        case ACBClientPresenceStatusAvailable:
            currentRow = 0;
            break;
        case ACBClientPresenceStatusBusy:
            currentRow = 1;
            break;
        case ACBClientPresenceStatusAway:
            currentRow = 2;
        default:
            currentRow = 3;
            break;
    }

    cell.backgroundColor = [UIColor darkGrayColor];

    if (row == currentRow)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        oldPath = indexPath;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];

    NSLog(@"Selected %li was %li", (long)indexPath.row, (long)oldPath.row);

    if (oldPath.row == indexPath.row)
    {
        return;
    }

    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldPath];
    if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark)
    {
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }

    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone)
    {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }

    ACBClientPresenceStatus newState;
    if (indexPath.row == 0)
    {
        newState = ACBClientPresenceStatusAvailable;
    }
    else if (indexPath.row == 1)
    {
        newState = ACBClientPresenceStatusBusy;
    }
    else
    {
        newState = ACBClientPresenceStatusAway;
    }
    NSString *customMessage = _customStateText.text;
    if (customMessage.length == 0)
    {
        customMessage = Nil;
    }
    [_delegate changeState:newState withMessage:customMessage];

    oldPath = indexPath;
}

@end