//
//  ConversationTableDataSource.h
//
//  Created by Ceri Hughes on 14/03/2013.
//

#import <Foundation/Foundation.h>
#import <ACBClientSDK/ACBClientConversation.h>

@interface ConversationTableDataSource : NSObject <UITableViewDataSource>

- (id) initWithConversation:(ACBClientConversation*)conversation;
- (ACBClientMessage*) sendMessage:(NSString*)text delegate:(id<ACBClientMessageDelegate>)delegate;
- (void) end;

@end