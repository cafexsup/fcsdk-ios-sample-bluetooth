//
//  AEDViewController.h
//
//  Created by swest on 07/03/2013.
//

#import <UIKit/UIKit.h>
#import "ChangeStateViewController.h"
#import "UCClientTabbedViewController.h"
#import <ACBClientSDK/ACBUC.h>
#import <ACBClientSDK/ACBClientPresence.h>

@interface AEDViewController : UIViewController<UCConsumer, ACBTopicDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *topicName;
@property (weak, nonatomic) IBOutlet UITextField *timeout;

@property (weak, nonatomic) IBOutlet UITextField *message;

@property ACBTopic *currentTopic;

@property (weak, nonatomic) IBOutlet UITextField *dataKey;
@property (weak, nonatomic) IBOutlet UILabel *deleteTopic;

@property (weak, nonatomic) IBOutlet UITextField *dataValue;

@property (weak, nonatomic) IBOutlet UITableView *topicView;
@property (weak, nonatomic) IBOutlet UITableView *logView2;

@end