//
//  ImSampleAppDelegate.h
//  ImPresenceSample
//
//  Created by Tom Greenwood on 16/01/2013.
//

#import <UIKit/UIKit.h>
#import "ConnectivityManager.h"
#import "LoginViewController.h"
#import "UCClientTabbedViewController.h"

@interface ImSampleAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly) ConnectivityManager *connectivityManager;
@property (strong) LoginViewController *loginViewController;
@property (strong) UCClientTabbedViewController *tabbedViewController;
@property BOOL userWantsToBeLoggedIn;

@end