//
//  main.m
//  ImPresenceSample
//
//  Created by Tom Greenwood on 16/01/2013.
//

#import <UIKit/UIKit.h>

#import "ImSampleAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ImSampleAppDelegate class]));
    }
}
